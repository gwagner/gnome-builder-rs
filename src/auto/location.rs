// Generated by gir (https://github.com/gtk-rs/gir @ bad7311422ee)
// from  (@ ???)
// from /usr/share/gir-1.0 (@ ???)
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

use glib::object::Cast;
use glib::object::IsA;
use glib::signal::connect_raw;
use glib::signal::SignalHandlerId;
use glib::translate::*;
use glib::StaticType;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib::wrapper! {
    #[doc(alias = "IdeLocation")]
    pub struct Location(Object<ffi::IdeLocation, ffi::IdeLocationClass>);

    match fn {
        type_ => || ffi::ide_location_get_type(),
    }
}

impl Location {
        pub const NONE: Option<&'static Location> = None;
    

    #[doc(alias = "ide_location_new")]
    pub fn new(file: &impl IsA<gio::File>, line: i32, line_offset: i32) -> Location {
        unsafe {
            from_glib_full(ffi::ide_location_new(file.as_ref().to_glib_none().0, line, line_offset))
        }
    }

    #[doc(alias = "ide_location_new_from_variant")]
    #[doc(alias = "new_from_variant")]
    pub fn from_variant(variant: Option<&glib::Variant>) -> Option<Location> {
        unsafe {
            from_glib_full(ffi::ide_location_new_from_variant(variant.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_location_new_with_offset")]
    #[doc(alias = "new_with_offset")]
    pub fn with_offset(file: &impl IsA<gio::File>, line: i32, line_offset: i32, offset: i32) -> Location {
        unsafe {
            from_glib_full(ffi::ide_location_new_with_offset(file.as_ref().to_glib_none().0, line, line_offset, offset))
        }
    }
}

pub trait LocationExt: 'static {
    #[doc(alias = "ide_location_compare")]
    fn compare(&self, b: &impl IsA<Location>) -> bool;

    #[doc(alias = "ide_location_dup")]
#[must_use]
    fn dup(&self) -> Option<Location>;

    #[doc(alias = "ide_location_dup_title")]
    fn dup_title(&self) -> Option<glib::GString>;

    #[doc(alias = "ide_location_equal")]
    fn equal(&self, b: &impl IsA<Location>) -> bool;

    #[doc(alias = "ide_location_get_file")]
    #[doc(alias = "get_file")]
    fn file(&self) -> Option<gio::File>;

    #[doc(alias = "ide_location_get_line")]
    #[doc(alias = "get_line")]
    fn line(&self) -> i32;

    #[doc(alias = "ide_location_get_line_offset")]
    #[doc(alias = "get_line_offset")]
    fn line_offset(&self) -> i32;

    #[doc(alias = "ide_location_get_offset")]
    #[doc(alias = "get_offset")]
    fn offset(&self) -> i32;

    #[doc(alias = "ide_location_hash")]
    fn hash(&self) -> u32;

    #[doc(alias = "ide_location_to_variant")]
    fn to_variant(&self) -> Option<glib::Variant>;

    fn title(&self) -> Option<glib::GString>;

    #[doc(alias = "title")]
    fn connect_title_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<Location>> LocationExt for O {
    fn compare(&self, b: &impl IsA<Location>) -> bool {
        unsafe {
            from_glib(ffi::ide_location_compare(self.as_ref().to_glib_none().0, b.as_ref().to_glib_none().0))
        }
    }

    fn dup(&self) -> Option<Location> {
        unsafe {
            from_glib_full(ffi::ide_location_dup(self.as_ref().to_glib_none().0))
        }
    }

    fn dup_title(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::ide_location_dup_title(self.as_ref().to_glib_none().0))
        }
    }

    fn equal(&self, b: &impl IsA<Location>) -> bool {
        unsafe {
            from_glib(ffi::ide_location_equal(self.as_ref().to_glib_none().0, b.as_ref().to_glib_none().0))
        }
    }

    fn file(&self) -> Option<gio::File> {
        unsafe {
            from_glib_none(ffi::ide_location_get_file(self.as_ref().to_glib_none().0))
        }
    }

    fn line(&self) -> i32 {
        unsafe {
            ffi::ide_location_get_line(self.as_ref().to_glib_none().0)
        }
    }

    fn line_offset(&self) -> i32 {
        unsafe {
            ffi::ide_location_get_line_offset(self.as_ref().to_glib_none().0)
        }
    }

    fn offset(&self) -> i32 {
        unsafe {
            ffi::ide_location_get_offset(self.as_ref().to_glib_none().0)
        }
    }

    fn hash(&self) -> u32 {
        unsafe {
            ffi::ide_location_hash(self.as_ref().to_glib_none().0)
        }
    }

    fn to_variant(&self) -> Option<glib::Variant> {
        unsafe {
            from_glib_full(ffi::ide_location_to_variant(self.as_ref().to_glib_none().0))
        }
    }

    fn title(&self) -> Option<glib::GString> {
        glib::ObjectExt::property(self.as_ref(), "title")
    }

    fn connect_title_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_title_trampoline<P: IsA<Location>, F: Fn(&P) + 'static>(this: *mut ffi::IdeLocation, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Location::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::title\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_title_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Location")
    }
}
