// Generated by gir (https://github.com/gtk-rs/gir @ bad7311422ee)
// from  (@ ???)
// from /usr/share/gir-1.0 (@ ???)
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

use crate::Context;
use crate::PreferencesWindow;
use glib::object::IsA;
use glib::translate::*;
use std::fmt;

glib::wrapper! {
    #[doc(alias = "IdePreferencesAddin")]
    pub struct PreferencesAddin(Interface<ffi::IdePreferencesAddin, ffi::IdePreferencesAddinInterface>);

    match fn {
        type_ => || ffi::ide_preferences_addin_get_type(),
    }
}

impl PreferencesAddin {
        pub const NONE: Option<&'static PreferencesAddin> = None;
    
}

pub trait PreferencesAddinExt: 'static {
    #[doc(alias = "ide_preferences_addin_load")]
    fn load(&self, preferences: &PreferencesWindow, context: Option<&Context>);

    #[doc(alias = "ide_preferences_addin_unload")]
    fn unload(&self, preferences: &PreferencesWindow, context: Option<&Context>);
}

impl<O: IsA<PreferencesAddin>> PreferencesAddinExt for O {
    fn load(&self, preferences: &PreferencesWindow, context: Option<&Context>) {
        unsafe {
            ffi::ide_preferences_addin_load(self.as_ref().to_glib_none().0, preferences.to_glib_none().0, context.to_glib_none().0);
        }
    }

    fn unload(&self, preferences: &PreferencesWindow, context: Option<&Context>) {
        unsafe {
            ffi::ide_preferences_addin_unload(self.as_ref().to_glib_none().0, preferences.to_glib_none().0, context.to_glib_none().0);
        }
    }
}

impl fmt::Display for PreferencesAddin {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("PreferencesAddin")
    }
}
