// Generated by gir (https://github.com/gtk-rs/gir @ bad7311422ee)
// from  (@ ???)
// from /usr/share/gir-1.0 (@ ???)
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

use crate::TextEdit;
use glib::object::IsA;
use glib::translate::*;
use std::fmt;

glib::wrapper! {
    #[doc(alias = "IdeLspCompletionItem")]
    pub struct LspCompletionItem(Object<ffi::IdeLspCompletionItem, ffi::IdeLspCompletionItemClass>);

    match fn {
        type_ => || ffi::ide_lsp_completion_item_get_type(),
    }
}

impl LspCompletionItem {
    #[doc(alias = "ide_lsp_completion_item_new")]
    pub fn new(variant: &glib::Variant) -> LspCompletionItem {
        unsafe {
            from_glib_full(ffi::ide_lsp_completion_item_new(variant.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_lsp_completion_item_display")]
    pub fn display(&self, cell: &gtk_source::CompletionCell, typed_text: &str) {
        unsafe {
            ffi::ide_lsp_completion_item_display(self.to_glib_none().0, cell.to_glib_none().0, typed_text.to_glib_none().0);
        }
    }

    #[doc(alias = "ide_lsp_completion_item_get_additional_text_edits")]
    #[doc(alias = "get_additional_text_edits")]
    pub fn additional_text_edits(&self, file: &impl IsA<gio::File>) -> Vec<TextEdit> {
        unsafe {
            FromGlibPtrContainer::from_glib_full(ffi::ide_lsp_completion_item_get_additional_text_edits(self.to_glib_none().0, file.as_ref().to_glib_none().0))
        }
    }

    #[doc(alias = "ide_lsp_completion_item_get_detail")]
    #[doc(alias = "get_detail")]
    pub fn detail(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::ide_lsp_completion_item_get_detail(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_lsp_completion_item_get_icon_name")]
    #[doc(alias = "get_icon_name")]
    pub fn icon_name(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::ide_lsp_completion_item_get_icon_name(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_lsp_completion_item_get_return_type")]
    #[doc(alias = "get_return_type")]
    pub fn return_type(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::ide_lsp_completion_item_get_return_type(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_lsp_completion_item_get_snippet")]
    #[doc(alias = "get_snippet")]
    pub fn snippet(&self) -> Option<gtk_source::Snippet> {
        unsafe {
            from_glib_full(ffi::ide_lsp_completion_item_get_snippet(self.to_glib_none().0))
        }
    }
}

impl fmt::Display for LspCompletionItem {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("LspCompletionItem")
    }
}
