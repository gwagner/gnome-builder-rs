// Generated by gir (https://github.com/gtk-rs/gir @ bad7311422ee)
// from  (@ ???)
// from /usr/share/gir-1.0 (@ ???)
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

use crate::BuildTarget;
use crate::Context;
use crate::Object;
use crate::Runtime;
use glib::object::Cast;
use glib::object::IsA;
use glib::signal::connect_raw;
use glib::signal::SignalHandlerId;
use glib::translate::*;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;
use std::pin::Pin;
use std::ptr;

glib::wrapper! {
    #[doc(alias = "IdeRunner")]
    pub struct Runner(Object<ffi::IdeRunner, ffi::IdeRunnerClass>) @extends Object;

    match fn {
        type_ => || ffi::ide_runner_get_type(),
    }
}

impl Runner {
        pub const NONE: Option<&'static Runner> = None;
    

    #[doc(alias = "ide_runner_new")]
    pub fn new(context: &Context) -> Runner {
        unsafe {
            from_glib_full(ffi::ide_runner_new(context.to_glib_none().0))
        }
    }
}

pub trait RunnerExt: 'static {
    #[doc(alias = "ide_runner_append_argv")]
    fn append_argv(&self, param: &str);

    #[doc(alias = "ide_runner_force_quit")]
    fn force_quit(&self);

    #[doc(alias = "ide_runner_get_argv")]
    #[doc(alias = "get_argv")]
    fn argv(&self) -> Vec<glib::GString>;

    #[doc(alias = "ide_runner_get_build_target")]
    #[doc(alias = "get_build_target")]
    fn build_target(&self) -> Option<BuildTarget>;

    #[doc(alias = "ide_runner_get_clear_env")]
    #[doc(alias = "get_clear_env")]
    fn is_clear_env(&self) -> bool;

    #[doc(alias = "ide_runner_get_cwd")]
    #[doc(alias = "get_cwd")]
    fn cwd(&self) -> Option<glib::GString>;

    #[doc(alias = "ide_runner_get_disable_pty")]
    #[doc(alias = "get_disable_pty")]
    fn is_disable_pty(&self) -> bool;

    //#[doc(alias = "ide_runner_get_environment")]
    //#[doc(alias = "get_environment")]
    //fn environment(&self) -> /*Ignored*/Option<Environment>;

    #[doc(alias = "ide_runner_get_failed")]
    #[doc(alias = "get_failed")]
    fn is_failed(&self) -> bool;

    #[doc(alias = "ide_runner_get_flags")]
    #[doc(alias = "get_flags")]
    fn flags(&self) -> gio::SubprocessFlags;

    #[doc(alias = "ide_runner_get_max_fd")]
    #[doc(alias = "get_max_fd")]
    fn max_fd(&self) -> i32;

    #[doc(alias = "ide_runner_get_pty")]
    #[doc(alias = "get_pty")]
    fn pty(&self) -> Option<vte::Pty>;

    #[doc(alias = "ide_runner_get_run_on_host")]
    #[doc(alias = "get_run_on_host")]
    fn is_run_on_host(&self) -> bool;

    #[doc(alias = "ide_runner_get_runtime")]
    #[doc(alias = "get_runtime")]
    fn runtime(&self) -> Option<Runtime>;

    #[doc(alias = "ide_runner_get_stderr")]
    #[doc(alias = "get_stderr")]
    fn stderr(&self) -> Option<gio::InputStream>;

    #[doc(alias = "ide_runner_get_stdin")]
    #[doc(alias = "get_stdin")]
    fn stdin(&self) -> Option<gio::OutputStream>;

    #[doc(alias = "ide_runner_get_stdout")]
    #[doc(alias = "get_stdout")]
    fn stdout(&self) -> Option<gio::InputStream>;

    #[doc(alias = "ide_runner_prepend_argv")]
    fn prepend_argv(&self, param: &str);

    #[doc(alias = "ide_runner_push_args")]
    fn push_args(&self, args: &[&str]);

    #[doc(alias = "ide_runner_run_async")]
    fn run_async<P: FnOnce(Result<(), glib::Error>) + 'static>(&self, cancellable: Option<&impl IsA<gio::Cancellable>>, callback: P);

    
    fn run_future(&self) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>>;

    #[doc(alias = "ide_runner_set_argv")]
    fn set_argv(&self, argv: &str);

    #[doc(alias = "ide_runner_set_build_target")]
    fn set_build_target(&self, build_target: Option<&impl IsA<BuildTarget>>);

    #[doc(alias = "ide_runner_set_clear_env")]
    fn set_clear_env(&self, clear_env: bool);

    #[doc(alias = "ide_runner_set_cwd")]
    fn set_cwd(&self, cwd: Option<&str>);

    #[doc(alias = "ide_runner_set_disable_pty")]
    fn set_disable_pty(&self, disable_pty: bool);

    #[doc(alias = "ide_runner_set_failed")]
    fn set_failed(&self, failed: bool);

    #[doc(alias = "ide_runner_set_flags")]
    fn set_flags(&self, flags: gio::SubprocessFlags);

    #[doc(alias = "ide_runner_set_pty")]
    fn set_pty(&self, pty: Option<&vte::Pty>);

    #[doc(alias = "ide_runner_set_run_on_host")]
    fn set_run_on_host(&self, run_on_host: bool);

    #[doc(alias = "ide_runner_take_fd")]
    fn take_fd(&self, source_fd: i32, dest_fd: i32) -> i32;

    #[doc(alias = "ide_runner_take_tty_fd")]
    fn take_tty_fd(&self, tty_fd: i32);

    #[doc(alias = "exited")]
    fn connect_exited<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    #[doc(alias = "spawned")]
    fn connect_spawned<F: Fn(&Self, &str) + 'static>(&self, f: F) -> SignalHandlerId;

    #[doc(alias = "argv")]
    fn connect_argv_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    #[doc(alias = "build-target")]
    fn connect_build_target_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    #[doc(alias = "clear-env")]
    fn connect_clear_env_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    #[doc(alias = "cwd")]
    fn connect_cwd_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    #[doc(alias = "disable-pty")]
    fn connect_disable_pty_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    #[doc(alias = "environment")]
    fn connect_environment_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    #[doc(alias = "failed")]
    fn connect_failed_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    #[doc(alias = "run-on-host")]
    fn connect_run_on_host_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<Runner>> RunnerExt for O {
    fn append_argv(&self, param: &str) {
        unsafe {
            ffi::ide_runner_append_argv(self.as_ref().to_glib_none().0, param.to_glib_none().0);
        }
    }

    fn force_quit(&self) {
        unsafe {
            ffi::ide_runner_force_quit(self.as_ref().to_glib_none().0);
        }
    }

    fn argv(&self) -> Vec<glib::GString> {
        unsafe {
            FromGlibPtrContainer::from_glib_full(ffi::ide_runner_get_argv(self.as_ref().to_glib_none().0))
        }
    }

    fn build_target(&self) -> Option<BuildTarget> {
        unsafe {
            from_glib_none(ffi::ide_runner_get_build_target(self.as_ref().to_glib_none().0))
        }
    }

    fn is_clear_env(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_runner_get_clear_env(self.as_ref().to_glib_none().0))
        }
    }

    fn cwd(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::ide_runner_get_cwd(self.as_ref().to_glib_none().0))
        }
    }

    fn is_disable_pty(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_runner_get_disable_pty(self.as_ref().to_glib_none().0))
        }
    }

    //fn environment(&self) -> /*Ignored*/Option<Environment> {
    //    unsafe { TODO: call ffi:ide_runner_get_environment() }
    //}

    fn is_failed(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_runner_get_failed(self.as_ref().to_glib_none().0))
        }
    }

    fn flags(&self) -> gio::SubprocessFlags {
        unsafe {
            from_glib(ffi::ide_runner_get_flags(self.as_ref().to_glib_none().0))
        }
    }

    fn max_fd(&self) -> i32 {
        unsafe {
            ffi::ide_runner_get_max_fd(self.as_ref().to_glib_none().0)
        }
    }

    fn pty(&self) -> Option<vte::Pty> {
        unsafe {
            from_glib_none(ffi::ide_runner_get_pty(self.as_ref().to_glib_none().0))
        }
    }

    fn is_run_on_host(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_runner_get_run_on_host(self.as_ref().to_glib_none().0))
        }
    }

    fn runtime(&self) -> Option<Runtime> {
        unsafe {
            from_glib_full(ffi::ide_runner_get_runtime(self.as_ref().to_glib_none().0))
        }
    }

    fn stderr(&self) -> Option<gio::InputStream> {
        unsafe {
            from_glib_full(ffi::ide_runner_get_stderr(self.as_ref().to_glib_none().0))
        }
    }

    fn stdin(&self) -> Option<gio::OutputStream> {
        unsafe {
            from_glib_full(ffi::ide_runner_get_stdin(self.as_ref().to_glib_none().0))
        }
    }

    fn stdout(&self) -> Option<gio::InputStream> {
        unsafe {
            from_glib_full(ffi::ide_runner_get_stdout(self.as_ref().to_glib_none().0))
        }
    }

    fn prepend_argv(&self, param: &str) {
        unsafe {
            ffi::ide_runner_prepend_argv(self.as_ref().to_glib_none().0, param.to_glib_none().0);
        }
    }

    fn push_args(&self, args: &[&str]) {
        unsafe {
            ffi::ide_runner_push_args(self.as_ref().to_glib_none().0, args.to_glib_none().0);
        }
    }

    fn run_async<P: FnOnce(Result<(), glib::Error>) + 'static>(&self, cancellable: Option<&impl IsA<gio::Cancellable>>, callback: P) {
        
                let main_context = glib::MainContext::ref_thread_default();
                let is_main_context_owner = main_context.is_owner();
                let has_acquired_main_context = (!is_main_context_owner)
                    .then(|| main_context.acquire().ok())
                    .flatten();
                assert!(
                    is_main_context_owner || has_acquired_main_context.is_some(),
                    "Async operations only allowed if the thread is owning the MainContext"
                );
        
        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn run_async_trampoline<P: FnOnce(Result<(), glib::Error>) + 'static>(_source_object: *mut glib::gobject_ffi::GObject, res: *mut gio::ffi::GAsyncResult, user_data: glib::ffi::gpointer) {
            let mut error = ptr::null_mut();
            let _ = ffi::ide_runner_run_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() { Ok(()) } else { Err(from_glib_full(error)) };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = run_async_trampoline::<P>;
        unsafe {
            ffi::ide_runner_run_async(self.as_ref().to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(user_data) as *mut _);
        }
    }

    
    fn run_future(&self) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {

        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.run_async(
                Some(cancellable),
                move |res| {
                    send.resolve(res);
                },
            );
        }))
    }

    fn set_argv(&self, argv: &str) {
        unsafe {
            ffi::ide_runner_set_argv(self.as_ref().to_glib_none().0, argv.to_glib_none().0);
        }
    }

    fn set_build_target(&self, build_target: Option<&impl IsA<BuildTarget>>) {
        unsafe {
            ffi::ide_runner_set_build_target(self.as_ref().to_glib_none().0, build_target.map(|p| p.as_ref()).to_glib_none().0);
        }
    }

    fn set_clear_env(&self, clear_env: bool) {
        unsafe {
            ffi::ide_runner_set_clear_env(self.as_ref().to_glib_none().0, clear_env.into_glib());
        }
    }

    fn set_cwd(&self, cwd: Option<&str>) {
        unsafe {
            ffi::ide_runner_set_cwd(self.as_ref().to_glib_none().0, cwd.to_glib_none().0);
        }
    }

    fn set_disable_pty(&self, disable_pty: bool) {
        unsafe {
            ffi::ide_runner_set_disable_pty(self.as_ref().to_glib_none().0, disable_pty.into_glib());
        }
    }

    fn set_failed(&self, failed: bool) {
        unsafe {
            ffi::ide_runner_set_failed(self.as_ref().to_glib_none().0, failed.into_glib());
        }
    }

    fn set_flags(&self, flags: gio::SubprocessFlags) {
        unsafe {
            ffi::ide_runner_set_flags(self.as_ref().to_glib_none().0, flags.into_glib());
        }
    }

    fn set_pty(&self, pty: Option<&vte::Pty>) {
        unsafe {
            ffi::ide_runner_set_pty(self.as_ref().to_glib_none().0, pty.to_glib_none().0);
        }
    }

    fn set_run_on_host(&self, run_on_host: bool) {
        unsafe {
            ffi::ide_runner_set_run_on_host(self.as_ref().to_glib_none().0, run_on_host.into_glib());
        }
    }

    fn take_fd(&self, source_fd: i32, dest_fd: i32) -> i32 {
        unsafe {
            ffi::ide_runner_take_fd(self.as_ref().to_glib_none().0, source_fd, dest_fd)
        }
    }

    fn take_tty_fd(&self, tty_fd: i32) {
        unsafe {
            ffi::ide_runner_take_tty_fd(self.as_ref().to_glib_none().0, tty_fd);
        }
    }

    fn connect_exited<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn exited_trampoline<P: IsA<Runner>, F: Fn(&P) + 'static>(this: *mut ffi::IdeRunner, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"exited\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(exited_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }

    fn connect_spawned<F: Fn(&Self, &str) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn spawned_trampoline<P: IsA<Runner>, F: Fn(&P, &str) + 'static>(this: *mut ffi::IdeRunner, object: *mut libc::c_char, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref(), &glib::GString::from_glib_borrow(object))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"spawned\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(spawned_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }

    fn connect_argv_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_argv_trampoline<P: IsA<Runner>, F: Fn(&P) + 'static>(this: *mut ffi::IdeRunner, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::argv\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_argv_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }

    fn connect_build_target_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_build_target_trampoline<P: IsA<Runner>, F: Fn(&P) + 'static>(this: *mut ffi::IdeRunner, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::build-target\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_build_target_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }

    fn connect_clear_env_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_clear_env_trampoline<P: IsA<Runner>, F: Fn(&P) + 'static>(this: *mut ffi::IdeRunner, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::clear-env\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_clear_env_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }

    fn connect_cwd_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_cwd_trampoline<P: IsA<Runner>, F: Fn(&P) + 'static>(this: *mut ffi::IdeRunner, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::cwd\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_cwd_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }

    fn connect_disable_pty_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_disable_pty_trampoline<P: IsA<Runner>, F: Fn(&P) + 'static>(this: *mut ffi::IdeRunner, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::disable-pty\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_disable_pty_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }

    fn connect_environment_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_environment_trampoline<P: IsA<Runner>, F: Fn(&P) + 'static>(this: *mut ffi::IdeRunner, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::environment\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_environment_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }

    fn connect_failed_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_failed_trampoline<P: IsA<Runner>, F: Fn(&P) + 'static>(this: *mut ffi::IdeRunner, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::failed\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_failed_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }

    fn connect_run_on_host_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_run_on_host_trampoline<P: IsA<Runner>, F: Fn(&P) + 'static>(this: *mut ffi::IdeRunner, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Runner::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::run-on-host\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_run_on_host_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }
}

impl fmt::Display for Runner {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Runner")
    }
}
