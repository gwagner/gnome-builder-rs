// Generated by gir (https://github.com/gtk-rs/gir @ bad7311422ee)
// from  (@ ???)
// from /usr/share/gir-1.0 (@ ???)
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

use crate::Object;
use glib::object::IsA;
use glib::object::ObjectType as ObjectType_;
use glib::signal::connect_raw;
use glib::signal::SignalHandlerId;
use glib::translate::*;
use glib::StaticType;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib::wrapper! {
    #[doc(alias = "IdeContext")]
    pub struct Context(Object<ffi::IdeContext, ffi::IdeContextClass>) @extends Object;

    match fn {
        type_ => || ffi::ide_context_get_type(),
    }
}

impl Context {
    #[doc(alias = "ide_context_new")]
    pub fn new() -> Context {
        unsafe {
            from_glib_full(ffi::ide_context_new())
        }
    }

    //#[doc(alias = "ide_context_addin_find_by_module_name")]
    //pub fn addin_find_by_module_name(&self, module_name: &str) -> /*Ignored*/Option<ContextAddin> {
    //    unsafe { TODO: call ffi:ide_context_addin_find_by_module_name() }
    //}

    #[doc(alias = "ide_context_build_file")]
    pub fn build_file(&self, path: Option<&str>) -> Option<gio::File> {
        unsafe {
            from_glib_full(ffi::ide_context_build_file(self.to_glib_none().0, path.to_glib_none().0))
        }
    }

    //#[doc(alias = "ide_context_build_filename")]
    //pub fn build_filename(&self, first_part: &str, : /*Unknown conversion*//*Unimplemented*/Basic: VarArgs) -> Option<glib::GString> {
    //    unsafe { TODO: call ffi:ide_context_build_filename() }
    //}

    //#[doc(alias = "ide_context_cache_file")]
    //pub fn cache_file(&self, first_part: Option<&str>, : /*Unknown conversion*//*Unimplemented*/Basic: VarArgs) -> Option<gio::File> {
    //    unsafe { TODO: call ffi:ide_context_cache_file() }
    //}

    //#[doc(alias = "ide_context_cache_filename")]
    //pub fn cache_filename(&self, first_part: &str, : /*Unknown conversion*//*Unimplemented*/Basic: VarArgs) -> Option<glib::GString> {
    //    unsafe { TODO: call ffi:ide_context_cache_filename() }
    //}

    #[doc(alias = "ide_context_dup_project_id")]
    pub fn dup_project_id(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::ide_context_dup_project_id(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_context_dup_title")]
    pub fn dup_title(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::ide_context_dup_title(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_context_has_project")]
    pub fn has_project(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_context_has_project(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_context_log")]
    pub fn log(&self, level: glib::LogLevelFlags, domain: &str, message: &str) {
        unsafe {
            ffi::ide_context_log(self.to_glib_none().0, level.into_glib(), domain.to_glib_none().0, message.to_glib_none().0);
        }
    }

    #[doc(alias = "ide_context_peek_child_typed")]
    pub fn peek_child_typed(&self, type_: glib::types::Type) -> Option<Object> {
        unsafe {
            from_glib_none(ffi::ide_context_peek_child_typed(self.to_glib_none().0, type_.into_glib()))
        }
    }

    //#[doc(alias = "ide_context_ref_project_settings")]
    //pub fn ref_project_settings(&self) -> /*Ignored*/Option<gio::Settings> {
    //    unsafe { TODO: call ffi:ide_context_ref_project_settings() }
    //}

    #[doc(alias = "ide_context_ref_workdir")]
    pub fn ref_workdir(&self) -> Option<gio::File> {
        unsafe {
            from_glib_full(ffi::ide_context_ref_workdir(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_context_set_project_id")]
    pub fn set_project_id(&self, project_id: &str) {
        unsafe {
            ffi::ide_context_set_project_id(self.to_glib_none().0, project_id.to_glib_none().0);
        }
    }

    #[doc(alias = "ide_context_set_title")]
    pub fn set_title(&self, title: Option<&str>) {
        unsafe {
            ffi::ide_context_set_title(self.to_glib_none().0, title.to_glib_none().0);
        }
    }

    #[doc(alias = "ide_context_set_workdir")]
    pub fn set_workdir(&self, workdir: &impl IsA<gio::File>) {
        unsafe {
            ffi::ide_context_set_workdir(self.to_glib_none().0, workdir.as_ref().to_glib_none().0);
        }
    }

    #[doc(alias = "project-id")]
    pub fn project_id(&self) -> Option<glib::GString> {
        glib::ObjectExt::property(self, "project-id")
    }

    pub fn title(&self) -> Option<glib::GString> {
        glib::ObjectExt::property(self, "title")
    }

    pub fn workdir(&self) -> Option<gio::File> {
        glib::ObjectExt::property(self, "workdir")
    }

    #[doc(alias = "log")]
    pub fn connect_log<F: Fn(&Self, u32, &str, &str) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn log_trampoline<F: Fn(&Context, u32, &str, &str) + 'static>(this: *mut ffi::IdeContext, severity: libc::c_uint, domain: *mut libc::c_char, message: *mut libc::c_char, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this), severity, &glib::GString::from_glib_borrow(domain), &glib::GString::from_glib_borrow(message))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"log\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(log_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "project-id")]
    pub fn connect_project_id_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_project_id_trampoline<F: Fn(&Context) + 'static>(this: *mut ffi::IdeContext, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::project-id\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_project_id_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "title")]
    pub fn connect_title_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_title_trampoline<F: Fn(&Context) + 'static>(this: *mut ffi::IdeContext, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::title\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_title_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "workdir")]
    pub fn connect_workdir_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_workdir_trampoline<F: Fn(&Context) + 'static>(this: *mut ffi::IdeContext, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::workdir\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_workdir_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }
}

impl Default for Context {
                     fn default() -> Self {
                         Self::new()
                     }
                 }

impl fmt::Display for Context {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Context")
    }
}
