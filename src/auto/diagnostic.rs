// Generated by gir (https://github.com/gtk-rs/gir @ bad7311422ee)
// from  (@ ???)
// from /usr/share/gir-1.0 (@ ???)
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

use crate::DiagnosticSeverity;
use crate::Location;
use crate::Object;
use crate::Range;
use crate::TextEdit;
use glib::object::Cast;
use glib::object::IsA;
use glib::signal::connect_raw;
use glib::signal::SignalHandlerId;
use glib::translate::*;
use glib::StaticType;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib::wrapper! {
    #[doc(alias = "IdeDiagnostic")]
    pub struct Diagnostic(Object<ffi::IdeDiagnostic, ffi::IdeDiagnosticClass>) @extends Object;

    match fn {
        type_ => || ffi::ide_diagnostic_get_type(),
    }
}

impl Diagnostic {
        pub const NONE: Option<&'static Diagnostic> = None;
    

    #[doc(alias = "ide_diagnostic_new")]
    pub fn new(severity: DiagnosticSeverity, message: &str, location: &impl IsA<Location>) -> Diagnostic {
        unsafe {
            from_glib_full(ffi::ide_diagnostic_new(severity.into_glib(), message.to_glib_none().0, location.as_ref().to_glib_none().0))
        }
    }

    #[doc(alias = "ide_diagnostic_new_from_variant")]
    #[doc(alias = "new_from_variant")]
    pub fn from_variant(variant: Option<&glib::Variant>) -> Option<Diagnostic> {
        unsafe {
            from_glib_full(ffi::ide_diagnostic_new_from_variant(variant.to_glib_none().0))
        }
    }
}

pub trait DiagnosticExt: 'static {
    #[doc(alias = "ide_diagnostic_add_fixit")]
    fn add_fixit(&self, fixit: &impl IsA<TextEdit>);

    #[doc(alias = "ide_diagnostic_add_range")]
    fn add_range(&self, range: &impl IsA<Range>);

    #[doc(alias = "ide_diagnostic_compare")]
    fn compare(&self, b: &impl IsA<Diagnostic>) -> bool;

    #[doc(alias = "ide_diagnostic_equal")]
    fn equal(&self, b: &impl IsA<Diagnostic>) -> bool;

    #[doc(alias = "ide_diagnostic_get_file")]
    #[doc(alias = "get_file")]
    fn file(&self) -> Option<gio::File>;

    #[doc(alias = "ide_diagnostic_get_fixit")]
    #[doc(alias = "get_fixit")]
    fn fixit(&self, index: u32) -> Option<TextEdit>;

    #[doc(alias = "ide_diagnostic_get_location")]
    #[doc(alias = "get_location")]
    fn location(&self) -> Option<Location>;

    //#[doc(alias = "ide_diagnostic_get_marked_kind")]
    //#[doc(alias = "get_marked_kind")]
    //fn marked_kind(&self) -> /*Ignored*/MarkedKind;

    #[doc(alias = "ide_diagnostic_get_n_fixits")]
    #[doc(alias = "get_n_fixits")]
    fn n_fixits(&self) -> u32;

    #[doc(alias = "ide_diagnostic_get_n_ranges")]
    #[doc(alias = "get_n_ranges")]
    fn n_ranges(&self) -> u32;

    #[doc(alias = "ide_diagnostic_get_range")]
    #[doc(alias = "get_range")]
    fn range(&self, index: u32) -> Option<Range>;

    #[doc(alias = "ide_diagnostic_get_severity")]
    #[doc(alias = "get_severity")]
    fn severity(&self) -> DiagnosticSeverity;

    #[doc(alias = "ide_diagnostic_get_text")]
    #[doc(alias = "get_text")]
    fn text(&self) -> Option<glib::GString>;

    #[doc(alias = "ide_diagnostic_get_text_for_display")]
    #[doc(alias = "get_text_for_display")]
    fn text_for_display(&self) -> Option<glib::GString>;

    #[doc(alias = "ide_diagnostic_hash")]
    fn hash(&self) -> u32;

    //#[doc(alias = "ide_diagnostic_set_marked_kind")]
    //fn set_marked_kind(&self, marked_kind: /*Ignored*/MarkedKind);

    #[doc(alias = "ide_diagnostic_take_fixit")]
    fn take_fixit(&self, fixit: &impl IsA<TextEdit>);

    #[doc(alias = "ide_diagnostic_take_range")]
    fn take_range(&self, range: &impl IsA<Range>);

    #[doc(alias = "ide_diagnostic_to_variant")]
    fn to_variant(&self) -> Option<glib::Variant>;

    #[doc(alias = "display-text")]
    fn display_text(&self) -> Option<glib::GString>;

    #[doc(alias = "display-text")]
    fn connect_display_text_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<Diagnostic>> DiagnosticExt for O {
    fn add_fixit(&self, fixit: &impl IsA<TextEdit>) {
        unsafe {
            ffi::ide_diagnostic_add_fixit(self.as_ref().to_glib_none().0, fixit.as_ref().to_glib_none().0);
        }
    }

    fn add_range(&self, range: &impl IsA<Range>) {
        unsafe {
            ffi::ide_diagnostic_add_range(self.as_ref().to_glib_none().0, range.as_ref().to_glib_none().0);
        }
    }

    fn compare(&self, b: &impl IsA<Diagnostic>) -> bool {
        unsafe {
            from_glib(ffi::ide_diagnostic_compare(self.as_ref().to_glib_none().0, b.as_ref().to_glib_none().0))
        }
    }

    fn equal(&self, b: &impl IsA<Diagnostic>) -> bool {
        unsafe {
            from_glib(ffi::ide_diagnostic_equal(self.as_ref().to_glib_none().0, b.as_ref().to_glib_none().0))
        }
    }

    fn file(&self) -> Option<gio::File> {
        unsafe {
            from_glib_none(ffi::ide_diagnostic_get_file(self.as_ref().to_glib_none().0))
        }
    }

    fn fixit(&self, index: u32) -> Option<TextEdit> {
        unsafe {
            from_glib_none(ffi::ide_diagnostic_get_fixit(self.as_ref().to_glib_none().0, index))
        }
    }

    fn location(&self) -> Option<Location> {
        unsafe {
            from_glib_none(ffi::ide_diagnostic_get_location(self.as_ref().to_glib_none().0))
        }
    }

    //fn marked_kind(&self) -> /*Ignored*/MarkedKind {
    //    unsafe { TODO: call ffi:ide_diagnostic_get_marked_kind() }
    //}

    fn n_fixits(&self) -> u32 {
        unsafe {
            ffi::ide_diagnostic_get_n_fixits(self.as_ref().to_glib_none().0)
        }
    }

    fn n_ranges(&self) -> u32 {
        unsafe {
            ffi::ide_diagnostic_get_n_ranges(self.as_ref().to_glib_none().0)
        }
    }

    fn range(&self, index: u32) -> Option<Range> {
        unsafe {
            from_glib_none(ffi::ide_diagnostic_get_range(self.as_ref().to_glib_none().0, index))
        }
    }

    fn severity(&self) -> DiagnosticSeverity {
        unsafe {
            from_glib(ffi::ide_diagnostic_get_severity(self.as_ref().to_glib_none().0))
        }
    }

    fn text(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::ide_diagnostic_get_text(self.as_ref().to_glib_none().0))
        }
    }

    fn text_for_display(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::ide_diagnostic_get_text_for_display(self.as_ref().to_glib_none().0))
        }
    }

    fn hash(&self) -> u32 {
        unsafe {
            ffi::ide_diagnostic_hash(self.as_ref().to_glib_none().0)
        }
    }

    //fn set_marked_kind(&self, marked_kind: /*Ignored*/MarkedKind) {
    //    unsafe { TODO: call ffi:ide_diagnostic_set_marked_kind() }
    //}

    fn take_fixit(&self, fixit: &impl IsA<TextEdit>) {
        unsafe {
            ffi::ide_diagnostic_take_fixit(self.as_ref().to_glib_none().0, fixit.as_ref().to_glib_full());
        }
    }

    fn take_range(&self, range: &impl IsA<Range>) {
        unsafe {
            ffi::ide_diagnostic_take_range(self.as_ref().to_glib_none().0, range.as_ref().to_glib_full());
        }
    }

    fn to_variant(&self) -> Option<glib::Variant> {
        unsafe {
            from_glib_full(ffi::ide_diagnostic_to_variant(self.as_ref().to_glib_none().0))
        }
    }

    fn display_text(&self) -> Option<glib::GString> {
        glib::ObjectExt::property(self.as_ref(), "display-text")
    }

    fn connect_display_text_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_display_text_trampoline<P: IsA<Diagnostic>, F: Fn(&P) + 'static>(this: *mut ffi::IdeDiagnostic, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(Diagnostic::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::display-text\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_display_text_trampoline::<Self, F> as *const ())), Box_::into_raw(f))
        }
    }
}

impl fmt::Display for Diagnostic {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Diagnostic")
    }
}
