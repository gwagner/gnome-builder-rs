// Generated by gir (https://github.com/gtk-rs/gir @ bad7311422ee)
// from  (@ ???)
// from /usr/share/gir-1.0 (@ ???)
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

use crate::Context;
use glib::translate::*;
use std::fmt;

glib::wrapper! {
    #[doc(alias = "IdePreferencesWindow")]
    pub struct PreferencesWindow(Object<ffi::IdePreferencesWindow, ffi::IdePreferencesWindowClass>);

    match fn {
        type_ => || ffi::ide_preferences_window_get_type(),
    }
}

impl PreferencesWindow {
    //#[doc(alias = "ide_preferences_window_new")]
    //pub fn new(mode: /*Ignored*/PreferencesMode, context: &Context) -> PreferencesWindow {
    //    unsafe { TODO: call ffi:ide_preferences_window_new() }
    //}

    //#[doc(alias = "ide_preferences_window_add_check")]
    //pub fn add_check(&self, item: /*Ignored*/&PreferenceItemEntry) {
    //    unsafe { TODO: call ffi:ide_preferences_window_add_check() }
    //}

    #[doc(alias = "ide_preferences_window_add_group")]
    pub fn add_group(&self, page: &str, name: &str, priority: i32, title: &str) {
        unsafe {
            ffi::ide_preferences_window_add_group(self.to_glib_none().0, page.to_glib_none().0, name.to_glib_none().0, priority, title.to_glib_none().0);
        }
    }

    //#[doc(alias = "ide_preferences_window_add_groups")]
    //pub fn add_groups(&self, groups: /*Ignored*/&[PreferenceGroupEntry], translation_domain: Option<&str>) {
    //    unsafe { TODO: call ffi:ide_preferences_window_add_groups() }
    //}

    //#[doc(alias = "ide_preferences_window_add_item")]
    //pub fn add_item(&self, page: &str, group: &str, name: &str, priority: i32, callback: /*Unimplemented*/Fn(&str, /*Ignored*/PreferenceItemEntry, /*Ignored*/adw::PreferencesGroup), user_data: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:ide_preferences_window_add_item() }
    //}

    //#[doc(alias = "ide_preferences_window_add_items")]
    //pub fn add_items(&self, items: /*Ignored*/&[PreferenceItemEntry], user_data: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:ide_preferences_window_add_items() }
    //}

    //#[doc(alias = "ide_preferences_window_add_pages")]
    //pub fn add_pages(&self, pages: /*Ignored*/&PreferencePageEntry, n_pages: usize, translation_domain: &str) {
    //    unsafe { TODO: call ffi:ide_preferences_window_add_pages() }
    //}

    //#[doc(alias = "ide_preferences_window_add_spin")]
    //pub fn add_spin(&self, item: /*Ignored*/&PreferenceItemEntry) {
    //    unsafe { TODO: call ffi:ide_preferences_window_add_spin() }
    //}

    //#[doc(alias = "ide_preferences_window_add_toggle")]
    //pub fn add_toggle(&self, item: /*Ignored*/&PreferenceItemEntry) {
    //    unsafe { TODO: call ffi:ide_preferences_window_add_toggle() }
    //}

    #[doc(alias = "ide_preferences_window_get_context")]
    #[doc(alias = "get_context")]
    pub fn context(&self) -> Option<Context> {
        unsafe {
            from_glib_none(ffi::ide_preferences_window_get_context(self.to_glib_none().0))
        }
    }

    //#[doc(alias = "ide_preferences_window_get_mode")]
    //#[doc(alias = "get_mode")]
    //pub fn mode(&self) -> /*Ignored*/PreferencesMode {
    //    unsafe { TODO: call ffi:ide_preferences_window_get_mode() }
    //}

    #[doc(alias = "ide_preferences_window_set_page")]
    pub fn set_page(&self, page: &str) {
        unsafe {
            ffi::ide_preferences_window_set_page(self.to_glib_none().0, page.to_glib_none().0);
        }
    }

    //#[doc(alias = "ide_preferences_window_check")]
    //pub fn check(page_name: &str, entry: /*Ignored*/&PreferenceItemEntry, group: /*Ignored*/&adw::PreferencesGroup, user_data: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:ide_preferences_window_check() }
    //}

    //#[doc(alias = "ide_preferences_window_combo")]
    //pub fn combo(page_name: &str, entry: /*Ignored*/&PreferenceItemEntry, group: /*Ignored*/&adw::PreferencesGroup, user_data: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:ide_preferences_window_combo() }
    //}

    //#[doc(alias = "ide_preferences_window_font")]
    //pub fn font(page_name: &str, entry: /*Ignored*/&PreferenceItemEntry, group: /*Ignored*/&adw::PreferencesGroup, user_data: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:ide_preferences_window_font() }
    //}

    //#[doc(alias = "ide_preferences_window_spin")]
    //pub fn spin(page_name: &str, entry: /*Ignored*/&PreferenceItemEntry, group: /*Ignored*/&adw::PreferencesGroup, user_data: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:ide_preferences_window_spin() }
    //}

    //#[doc(alias = "ide_preferences_window_toggle")]
    //pub fn toggle(page_name: &str, entry: /*Ignored*/&PreferenceItemEntry, group: /*Ignored*/&adw::PreferencesGroup, user_data: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:ide_preferences_window_toggle() }
    //}
}

impl fmt::Display for PreferencesWindow {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("PreferencesWindow")
    }
}
