// Generated by gir (https://github.com/gtk-rs/gir @ bad7311422ee)
// from  (@ ???)
// from /usr/share/gir-1.0 (@ ???)
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

use crate::BufferAddin;
use crate::BufferChangeMonitor;
use crate::BufferState;
use crate::CodeAction;
use crate::Context;
use crate::Diagnostics;
use crate::FileSettings;
use crate::Formatter;
use crate::FormatterOptions;
use crate::Location;
use crate::Range;
use crate::RenameProvider;
use crate::Symbol;
use crate::SymbolResolver;
use glib::object::IsA;
use glib::object::ObjectType as ObjectType_;
use glib::signal::connect_raw;
use glib::signal::SignalHandlerId;
use glib::translate::*;
use glib::StaticType;
use glib::ToValue;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;
use std::pin::Pin;
use std::ptr;

glib::wrapper! {
    #[doc(alias = "IdeBuffer")]
    pub struct Buffer(Object<ffi::IdeBuffer, ffi::IdeBufferClass>);

    match fn {
        type_ => || ffi::ide_buffer_get_type(),
    }
}

impl Buffer {
    //#[doc(alias = "ide_buffer_add_commit_funcs")]
    //pub fn add_commit_funcs(&self, before_insert_text: Option<Box_<dyn FnOnce(&Buffer, u32, u32) + 'static>>, after_insert_text: Option<Box_<dyn FnOnce(&Buffer, u32, u32) + 'static>>, before_delete_range: Option<Box_<dyn FnOnce(&Buffer, u32, u32) + 'static>>, after_delete_range: Option<Box_<dyn Fn(&Buffer, u32, u32) + 'static>>) -> u32 {
    //    unsafe { TODO: call ffi:ide_buffer_add_commit_funcs() }
    //}

    #[doc(alias = "ide_buffer_addin_find_by_module_name")]
    pub fn addin_find_by_module_name(&self, module_name: &str) -> Option<BufferAddin> {
        unsafe {
            from_glib_none(ffi::ide_buffer_addin_find_by_module_name(self.to_glib_none().0, module_name.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_code_action_query_async")]
    pub fn code_action_query_async<P: FnOnce(Result<Vec<CodeAction>, glib::Error>) + 'static>(&self, cancellable: Option<&impl IsA<gio::Cancellable>>, callback: P) {
        
                let main_context = glib::MainContext::ref_thread_default();
                let is_main_context_owner = main_context.is_owner();
                let has_acquired_main_context = (!is_main_context_owner)
                    .then(|| main_context.acquire().ok())
                    .flatten();
                assert!(
                    is_main_context_owner || has_acquired_main_context.is_some(),
                    "Async operations only allowed if the thread is owning the MainContext"
                );
        
        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn code_action_query_async_trampoline<P: FnOnce(Result<Vec<CodeAction>, glib::Error>) + 'static>(_source_object: *mut glib::gobject_ffi::GObject, res: *mut gio::ffi::GAsyncResult, user_data: glib::ffi::gpointer) {
            let mut error = ptr::null_mut();
            let ret = ffi::ide_buffer_code_action_query_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() { Ok(FromGlibPtrContainer::from_glib_full(ret)) } else { Err(from_glib_full(error)) };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = code_action_query_async_trampoline::<P>;
        unsafe {
            ffi::ide_buffer_code_action_query_async(self.to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(user_data) as *mut _);
        }
    }

    
    pub fn code_action_query_future(&self) -> Pin<Box_<dyn std::future::Future<Output = Result<Vec<CodeAction>, glib::Error>> + 'static>> {

        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.code_action_query_async(
                Some(cancellable),
                move |res| {
                    send.resolve(res);
                },
            );
        }))
    }

    #[doc(alias = "ide_buffer_dup_content")]
    pub fn dup_content(&self) -> Option<glib::Bytes> {
        unsafe {
            from_glib_full(ffi::ide_buffer_dup_content(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_dup_title")]
    pub fn dup_title(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::ide_buffer_dup_title(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_dup_uri")]
    pub fn dup_uri(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::ide_buffer_dup_uri(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_format_selection_async")]
    pub fn format_selection_async<P: FnOnce(Result<(), glib::Error>) + 'static>(&self, options: &FormatterOptions, cancellable: Option<&impl IsA<gio::Cancellable>>, callback: P) {
        
                let main_context = glib::MainContext::ref_thread_default();
                let is_main_context_owner = main_context.is_owner();
                let has_acquired_main_context = (!is_main_context_owner)
                    .then(|| main_context.acquire().ok())
                    .flatten();
                assert!(
                    is_main_context_owner || has_acquired_main_context.is_some(),
                    "Async operations only allowed if the thread is owning the MainContext"
                );
        
        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn format_selection_async_trampoline<P: FnOnce(Result<(), glib::Error>) + 'static>(_source_object: *mut glib::gobject_ffi::GObject, res: *mut gio::ffi::GAsyncResult, user_data: glib::ffi::gpointer) {
            let mut error = ptr::null_mut();
            let _ = ffi::ide_buffer_format_selection_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() { Ok(()) } else { Err(from_glib_full(error)) };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = format_selection_async_trampoline::<P>;
        unsafe {
            ffi::ide_buffer_format_selection_async(self.to_glib_none().0, options.to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(user_data) as *mut _);
        }
    }

    
    pub fn format_selection_future(&self, options: &FormatterOptions) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {

        let options = options.clone();
        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.format_selection_async(
                &options,
                Some(cancellable),
                move |res| {
                    send.resolve(res);
                },
            );
        }))
    }

    #[doc(alias = "ide_buffer_get_change_count")]
    #[doc(alias = "get_change_count")]
    pub fn change_count(&self) -> u32 {
        unsafe {
            ffi::ide_buffer_get_change_count(self.to_glib_none().0)
        }
    }

    #[doc(alias = "ide_buffer_get_change_monitor")]
    #[doc(alias = "get_change_monitor")]
    pub fn change_monitor(&self) -> Option<BufferChangeMonitor> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_change_monitor(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_changed_on_volume")]
    #[doc(alias = "get_changed_on_volume")]
    pub fn is_changed_on_volume(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_buffer_get_changed_on_volume(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_charset")]
    #[doc(alias = "get_charset")]
    pub fn charset(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_charset(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_diagnostics")]
    #[doc(alias = "get_diagnostics")]
    pub fn diagnostics(&self) -> Option<Diagnostics> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_diagnostics(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_failed")]
    #[doc(alias = "get_failed")]
    pub fn is_failed(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_buffer_get_failed(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_failure")]
    #[doc(alias = "get_failure")]
    pub fn failure(&self) -> Option<glib::Error> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_failure(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_file")]
    #[doc(alias = "get_file")]
    pub fn file(&self) -> Option<gio::File> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_file(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_file_settings")]
    #[doc(alias = "get_file_settings")]
    pub fn file_settings(&self) -> Option<FileSettings> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_file_settings(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_formatter")]
    #[doc(alias = "get_formatter")]
    pub fn formatter(&self) -> Option<Formatter> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_formatter(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_highlight_diagnostics")]
    #[doc(alias = "get_highlight_diagnostics")]
    pub fn is_highlight_diagnostics(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_buffer_get_highlight_diagnostics(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_insert_location")]
    #[doc(alias = "get_insert_location")]
    pub fn insert_location(&self) -> Option<Location> {
        unsafe {
            from_glib_full(ffi::ide_buffer_get_insert_location(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_is_temporary")]
    #[doc(alias = "get_is_temporary")]
    pub fn is_temporary(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_buffer_get_is_temporary(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_iter_at_location")]
    #[doc(alias = "get_iter_at_location")]
    pub fn iter_at_location(&self, location: &impl IsA<Location>) -> gtk::TextIter {
        unsafe {
            let mut iter = gtk::TextIter::uninitialized();
            ffi::ide_buffer_get_iter_at_location(self.to_glib_none().0, iter.to_glib_none_mut().0, location.as_ref().to_glib_none().0);
            iter
        }
    }

    #[doc(alias = "ide_buffer_get_iter_location")]
    #[doc(alias = "get_iter_location")]
    pub fn iter_location(&self, iter: &gtk::TextIter) -> Option<Location> {
        unsafe {
            from_glib_full(ffi::ide_buffer_get_iter_location(self.to_glib_none().0, iter.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_language_id")]
    #[doc(alias = "get_language_id")]
    pub fn language_id(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_language_id(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_line_text")]
    #[doc(alias = "get_line_text")]
    pub fn line_text(&self, line: u32) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::ide_buffer_get_line_text(self.to_glib_none().0, line))
        }
    }

    #[doc(alias = "ide_buffer_get_loading")]
    #[doc(alias = "get_loading")]
    pub fn is_loading(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_buffer_get_loading(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_newline_type")]
    #[doc(alias = "get_newline_type")]
    pub fn newline_type(&self) -> gtk_source::NewlineType {
        unsafe {
            from_glib(ffi::ide_buffer_get_newline_type(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_read_only")]
    #[doc(alias = "get_read_only")]
    pub fn is_read_only(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_buffer_get_read_only(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_rename_provider")]
    #[doc(alias = "get_rename_provider")]
    pub fn rename_provider(&self) -> Option<RenameProvider> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_rename_provider(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_selection_bounds")]
    #[doc(alias = "get_selection_bounds")]
    pub fn selection_bounds(&self) -> (gtk::TextIter, gtk::TextIter) {
        unsafe {
            let mut insert = gtk::TextIter::uninitialized();
            let mut selection = gtk::TextIter::uninitialized();
            ffi::ide_buffer_get_selection_bounds(self.to_glib_none().0, insert.to_glib_none_mut().0, selection.to_glib_none_mut().0);
            (insert, selection)
        }
    }

    #[doc(alias = "ide_buffer_get_selection_range")]
    #[doc(alias = "get_selection_range")]
    pub fn selection_range(&self) -> Option<Range> {
        unsafe {
            from_glib_full(ffi::ide_buffer_get_selection_range(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_state")]
    #[doc(alias = "get_state")]
    pub fn state(&self) -> BufferState {
        unsafe {
            from_glib(ffi::ide_buffer_get_state(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_style_scheme_name")]
    #[doc(alias = "get_style_scheme_name")]
    pub fn style_scheme_name(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::ide_buffer_get_style_scheme_name(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_symbol_at_location_async")]
    #[doc(alias = "get_symbol_at_location_async")]
    pub fn symbol_at_location_async<P: FnOnce(Result<Symbol, glib::Error>) + 'static>(&self, location: &gtk::TextIter, cancellable: Option<&impl IsA<gio::Cancellable>>, callback: P) {
        
                let main_context = glib::MainContext::ref_thread_default();
                let is_main_context_owner = main_context.is_owner();
                let has_acquired_main_context = (!is_main_context_owner)
                    .then(|| main_context.acquire().ok())
                    .flatten();
                assert!(
                    is_main_context_owner || has_acquired_main_context.is_some(),
                    "Async operations only allowed if the thread is owning the MainContext"
                );
        
        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn symbol_at_location_async_trampoline<P: FnOnce(Result<Symbol, glib::Error>) + 'static>(_source_object: *mut glib::gobject_ffi::GObject, res: *mut gio::ffi::GAsyncResult, user_data: glib::ffi::gpointer) {
            let mut error = ptr::null_mut();
            let ret = ffi::ide_buffer_get_symbol_at_location_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() { Ok(from_glib_full(ret)) } else { Err(from_glib_full(error)) };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = symbol_at_location_async_trampoline::<P>;
        unsafe {
            ffi::ide_buffer_get_symbol_at_location_async(self.to_glib_none().0, location.to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(user_data) as *mut _);
        }
    }

    
    pub fn symbol_at_location_future(&self, location: &gtk::TextIter) -> Pin<Box_<dyn std::future::Future<Output = Result<Symbol, glib::Error>> + 'static>> {

        let location = location.clone();
        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.symbol_at_location_async(
                &location,
                Some(cancellable),
                move |res| {
                    send.resolve(res);
                },
            );
        }))
    }

    #[doc(alias = "ide_buffer_get_symbol_resolvers")]
    #[doc(alias = "get_symbol_resolvers")]
    pub fn symbol_resolvers(&self) -> Vec<SymbolResolver> {
        unsafe {
            FromGlibPtrContainer::from_glib_full(ffi::ide_buffer_get_symbol_resolvers(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_get_word_at_iter")]
    #[doc(alias = "get_word_at_iter")]
    pub fn word_at_iter(&self, iter: &gtk::TextIter) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::ide_buffer_get_word_at_iter(self.to_glib_none().0, iter.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_has_diagnostics")]
    pub fn has_diagnostics(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_buffer_has_diagnostics(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_has_symbol_resolvers")]
    pub fn has_symbol_resolvers(&self) -> bool {
        unsafe {
            from_glib(ffi::ide_buffer_has_symbol_resolvers(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_hold")]
#[must_use]
    pub fn hold(&self) -> Option<Buffer> {
        unsafe {
            from_glib_full(ffi::ide_buffer_hold(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_ref_context")]
    pub fn ref_context(&self) -> Option<Context> {
        unsafe {
            from_glib_full(ffi::ide_buffer_ref_context(self.to_glib_none().0))
        }
    }

    #[doc(alias = "ide_buffer_rehighlight")]
    pub fn rehighlight(&self) {
        unsafe {
            ffi::ide_buffer_rehighlight(self.to_glib_none().0);
        }
    }

    #[doc(alias = "ide_buffer_release")]
    pub fn release(&self) {
        unsafe {
            ffi::ide_buffer_release(self.to_glib_none().0);
        }
    }

    #[doc(alias = "ide_buffer_remove_commit_funcs")]
    pub fn remove_commit_funcs(&self, commit_funcs_handler: u32) {
        unsafe {
            ffi::ide_buffer_remove_commit_funcs(self.to_glib_none().0, commit_funcs_handler);
        }
    }

    //#[doc(alias = "ide_buffer_save_file_async")]
    //pub fn save_file_async<P: FnOnce(Result<(), glib::Error>) + 'static>(&self, file: Option<&impl IsA<gio::File>>, cancellable: Option<&impl IsA<gio::Cancellable>>, callback: P) {
    //    unsafe { TODO: call ffi:ide_buffer_save_file_async() }
    //}

    //
    //pub fn save_file_future(&self, file: Option<&(impl IsA<gio::File> + Clone + 'static)>) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {

        //let file = file.map(ToOwned::to_owned);
        //Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
        //    obj.save_file_async(
        //        file.as_ref().map(::std::borrow::Borrow::borrow),
        //        Some(cancellable),
        //        move |res| {
        //            send.resolve(res);
        //        },
        //    );
        //}))
    //}

    #[doc(alias = "ide_buffer_set_change_monitor")]
    pub fn set_change_monitor(&self, change_monitor: Option<&impl IsA<BufferChangeMonitor>>) {
        unsafe {
            ffi::ide_buffer_set_change_monitor(self.to_glib_none().0, change_monitor.map(|p| p.as_ref()).to_glib_none().0);
        }
    }

    #[doc(alias = "ide_buffer_set_charset")]
    pub fn set_charset(&self, charset: &str) {
        unsafe {
            ffi::ide_buffer_set_charset(self.to_glib_none().0, charset.to_glib_none().0);
        }
    }

    #[doc(alias = "ide_buffer_set_diagnostics")]
    pub fn set_diagnostics(&self, diagnostics: Option<&impl IsA<Diagnostics>>) {
        unsafe {
            ffi::ide_buffer_set_diagnostics(self.to_glib_none().0, diagnostics.map(|p| p.as_ref()).to_glib_none().0);
        }
    }

    #[doc(alias = "ide_buffer_set_highlight_diagnostics")]
    pub fn set_highlight_diagnostics(&self, highlight_diagnostics: bool) {
        unsafe {
            ffi::ide_buffer_set_highlight_diagnostics(self.to_glib_none().0, highlight_diagnostics.into_glib());
        }
    }

    #[doc(alias = "ide_buffer_set_language_id")]
    pub fn set_language_id(&self, language_id: &str) {
        unsafe {
            ffi::ide_buffer_set_language_id(self.to_glib_none().0, language_id.to_glib_none().0);
        }
    }

    #[doc(alias = "ide_buffer_set_newline_type")]
    pub fn set_newline_type(&self, newline_type: gtk_source::NewlineType) {
        unsafe {
            ffi::ide_buffer_set_newline_type(self.to_glib_none().0, newline_type.into_glib());
        }
    }

    #[doc(alias = "ide_buffer_set_style_scheme_name")]
    pub fn set_style_scheme_name(&self, style_scheme_name: Option<&str>) {
        unsafe {
            ffi::ide_buffer_set_style_scheme_name(self.to_glib_none().0, style_scheme_name.to_glib_none().0);
        }
    }

    #[doc(alias = "enable-addins")]
    pub fn enables_addins(&self) -> bool {
        glib::ObjectExt::property(self, "enable-addins")
    }

    pub fn set_file<P: IsA<gio::File>>(&self, file: Option<&P>) {
        glib::ObjectExt::set_property(self,"file", &file)
    }

    pub fn title(&self) -> Option<glib::GString> {
        glib::ObjectExt::property(self, "title")
    }

    #[doc(alias = "change-settled")]
    pub fn connect_change_settled<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn change_settled_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"change-settled\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(change_settled_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "line-flags-changed")]
    pub fn connect_line_flags_changed<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn line_flags_changed_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"line-flags-changed\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(line_flags_changed_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "loaded")]
    pub fn connect_loaded<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn loaded_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"loaded\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(loaded_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "request-scroll-to-insert")]
    pub fn connect_request_scroll_to_insert<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn request_scroll_to_insert_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"request-scroll-to-insert\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(request_scroll_to_insert_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "change-monitor")]
    pub fn connect_change_monitor_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_change_monitor_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::change-monitor\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_change_monitor_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "changed-on-volume")]
    pub fn connect_changed_on_volume_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_changed_on_volume_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::changed-on-volume\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_changed_on_volume_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "charset")]
    pub fn connect_charset_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_charset_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::charset\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_charset_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "diagnostics")]
    pub fn connect_diagnostics_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_diagnostics_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::diagnostics\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_diagnostics_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "failed")]
    pub fn connect_failed_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_failed_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::failed\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_failed_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "file")]
    pub fn connect_file_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_file_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::file\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_file_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "file-settings")]
    pub fn connect_file_settings_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_file_settings_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::file-settings\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_file_settings_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "has-diagnostics")]
    pub fn connect_has_diagnostics_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_has_diagnostics_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::has-diagnostics\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_has_diagnostics_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "has-symbol-resolvers")]
    pub fn connect_has_symbol_resolvers_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_has_symbol_resolvers_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::has-symbol-resolvers\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_has_symbol_resolvers_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "highlight-diagnostics")]
    pub fn connect_highlight_diagnostics_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_highlight_diagnostics_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::highlight-diagnostics\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_highlight_diagnostics_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "language-id")]
    pub fn connect_language_id_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_language_id_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::language-id\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_language_id_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "newline-type")]
    pub fn connect_newline_type_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_newline_type_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::newline-type\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_newline_type_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "read-only")]
    pub fn connect_read_only_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_read_only_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::read-only\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_read_only_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "state")]
    pub fn connect_state_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_state_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::state\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_state_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "style-scheme-name")]
    pub fn connect_style_scheme_name_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_style_scheme_name_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::style-scheme-name\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_style_scheme_name_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }

    #[doc(alias = "title")]
    pub fn connect_title_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_title_trampoline<F: Fn(&Buffer) + 'static>(this: *mut ffi::IdeBuffer, _param_spec: glib::ffi::gpointer, f: glib::ffi::gpointer) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::title\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(notify_title_trampoline::<F> as *const ())), Box_::into_raw(f))
        }
    }
}

impl fmt::Display for Buffer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Buffer")
    }
}
