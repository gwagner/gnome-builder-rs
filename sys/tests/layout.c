// Generated by gir (https://github.com/gtk-rs/gir @ bad7311422ee)
// from /usr/share/gir-1.0 (@ ???)
// from .. (@ ???)
// from ../gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

#include "manual.h"
#include <stdalign.h>
#include <stdio.h>

int main() {
    printf("%s;%zu;%zu\n", "IdeAnimationClass", sizeof(IdeAnimationClass), alignof(IdeAnimationClass));
    printf("%s;%zu;%zu\n", "IdeAnimationMode", sizeof(IdeAnimationMode), alignof(IdeAnimationMode));
    printf("%s;%zu;%zu\n", "IdeApplicationAddinInterface", sizeof(IdeApplicationAddinInterface), alignof(IdeApplicationAddinInterface));
    printf("%s;%zu;%zu\n", "IdeApplicationClass", sizeof(IdeApplicationClass), alignof(IdeApplicationClass));
    printf("%s;%zu;%zu\n", "IdeArtifactKind", sizeof(IdeArtifactKind), alignof(IdeArtifactKind));
    printf("%s;%zu;%zu\n", "IdeBindingGroupClass", sizeof(IdeBindingGroupClass), alignof(IdeBindingGroupClass));
    printf("%s;%zu;%zu\n", "IdeBufferAddinInterface", sizeof(IdeBufferAddinInterface), alignof(IdeBufferAddinInterface));
    printf("%s;%zu;%zu\n", "IdeBufferChangeMonitor", sizeof(IdeBufferChangeMonitor), alignof(IdeBufferChangeMonitor));
    printf("%s;%zu;%zu\n", "IdeBufferChangeMonitorClass", sizeof(IdeBufferChangeMonitorClass), alignof(IdeBufferChangeMonitorClass));
    printf("%s;%zu;%zu\n", "IdeBufferClass", sizeof(IdeBufferClass), alignof(IdeBufferClass));
    printf("%s;%zu;%zu\n", "IdeBufferLineChange", sizeof(IdeBufferLineChange), alignof(IdeBufferLineChange));
    printf("%s;%zu;%zu\n", "IdeBufferManagerClass", sizeof(IdeBufferManagerClass), alignof(IdeBufferManagerClass));
    printf("%s;%zu;%zu\n", "IdeBufferOpenFlags", sizeof(IdeBufferOpenFlags), alignof(IdeBufferOpenFlags));
    printf("%s;%zu;%zu\n", "IdeBufferState", sizeof(IdeBufferState), alignof(IdeBufferState));
    printf("%s;%zu;%zu\n", "IdeBuildError", sizeof(IdeBuildError), alignof(IdeBuildError));
    printf("%s;%zu;%zu\n", "IdeBuildLocality", sizeof(IdeBuildLocality), alignof(IdeBuildLocality));
    printf("%s;%zu;%zu\n", "IdeBuildLogStream", sizeof(IdeBuildLogStream), alignof(IdeBuildLogStream));
    printf("%s;%zu;%zu\n", "IdeBuildManagerClass", sizeof(IdeBuildManagerClass), alignof(IdeBuildManagerClass));
    printf("%s;%zu;%zu\n", "IdeBuildSystemDiscoveryInterface", sizeof(IdeBuildSystemDiscoveryInterface), alignof(IdeBuildSystemDiscoveryInterface));
    printf("%s;%zu;%zu\n", "IdeBuildSystemInterface", sizeof(IdeBuildSystemInterface), alignof(IdeBuildSystemInterface));
    printf("%s;%zu;%zu\n", "IdeBuildTargetInterface", sizeof(IdeBuildTargetInterface), alignof(IdeBuildTargetInterface));
    printf("%s;%zu;%zu\n", "IdeBuildTargetProviderInterface", sizeof(IdeBuildTargetProviderInterface), alignof(IdeBuildTargetProviderInterface));
    printf("%s;%zu;%zu\n", "IdeCellRendererFancyClass", sizeof(IdeCellRendererFancyClass), alignof(IdeCellRendererFancyClass));
    printf("%s;%zu;%zu\n", "IdeCodeActionInterface", sizeof(IdeCodeActionInterface), alignof(IdeCodeActionInterface));
    printf("%s;%zu;%zu\n", "IdeCodeActionProviderInterface", sizeof(IdeCodeActionProviderInterface), alignof(IdeCodeActionProviderInterface));
    printf("%s;%zu;%zu\n", "IdeCodeIndexEntriesInterface", sizeof(IdeCodeIndexEntriesInterface), alignof(IdeCodeIndexEntriesInterface));
    printf("%s;%zu;%zu\n", "IdeCodeIndexerInterface", sizeof(IdeCodeIndexerInterface), alignof(IdeCodeIndexerInterface));
    printf("%s;%zu;%zu\n", "IdeCommandInterface", sizeof(IdeCommandInterface), alignof(IdeCommandInterface));
    printf("%s;%zu;%zu\n", "IdeCommandManagerClass", sizeof(IdeCommandManagerClass), alignof(IdeCommandManagerClass));
    printf("%s;%zu;%zu\n", "IdeCommandProviderInterface", sizeof(IdeCommandProviderInterface), alignof(IdeCommandProviderInterface));
    printf("%s;%zu;%zu\n", "IdeCompileCommandsClass", sizeof(IdeCompileCommandsClass), alignof(IdeCompileCommandsClass));
    printf("%s;%zu;%zu\n", "IdeConfig", sizeof(IdeConfig), alignof(IdeConfig));
    printf("%s;%zu;%zu\n", "IdeConfigClass", sizeof(IdeConfigClass), alignof(IdeConfigClass));
    printf("%s;%zu;%zu\n", "IdeConfigManagerClass", sizeof(IdeConfigManagerClass), alignof(IdeConfigManagerClass));
    printf("%s;%zu;%zu\n", "IdeConfigProviderInterface", sizeof(IdeConfigProviderInterface), alignof(IdeConfigProviderInterface));
    printf("%s;%zu;%zu\n", "IdeConfigViewAddinInterface", sizeof(IdeConfigViewAddinInterface), alignof(IdeConfigViewAddinInterface));
    printf("%s;%zu;%zu\n", "IdeContextAddinInterface", sizeof(IdeContextAddinInterface), alignof(IdeContextAddinInterface));
    printf("%s;%zu;%zu\n", "IdeContextClass", sizeof(IdeContextClass), alignof(IdeContextClass));
    printf("%s;%zu;%zu\n", "IdeDebugManagerClass", sizeof(IdeDebugManagerClass), alignof(IdeDebugManagerClass));
    printf("%s;%zu;%zu\n", "IdeDebugger", sizeof(IdeDebugger), alignof(IdeDebugger));
    printf("%s;%zu;%zu\n", "IdeDebuggerAddress", sizeof(IdeDebuggerAddress), alignof(IdeDebuggerAddress));
    printf("%s;%zu;%zu\n", "IdeDebuggerAddressRange", sizeof(IdeDebuggerAddressRange), alignof(IdeDebuggerAddressRange));
    printf("%s;%zu;%zu\n", "IdeDebuggerBreakMode", sizeof(IdeDebuggerBreakMode), alignof(IdeDebuggerBreakMode));
    printf("%s;%zu;%zu\n", "IdeDebuggerBreakpoint", sizeof(IdeDebuggerBreakpoint), alignof(IdeDebuggerBreakpoint));
    printf("%s;%zu;%zu\n", "IdeDebuggerBreakpointChange", sizeof(IdeDebuggerBreakpointChange), alignof(IdeDebuggerBreakpointChange));
    printf("%s;%zu;%zu\n", "IdeDebuggerBreakpointClass", sizeof(IdeDebuggerBreakpointClass), alignof(IdeDebuggerBreakpointClass));
    printf("%s;%zu;%zu\n", "IdeDebuggerBreakpointsClass", sizeof(IdeDebuggerBreakpointsClass), alignof(IdeDebuggerBreakpointsClass));
    printf("%s;%zu;%zu\n", "IdeDebuggerClass", sizeof(IdeDebuggerClass), alignof(IdeDebuggerClass));
    printf("%s;%zu;%zu\n", "IdeDebuggerDisposition", sizeof(IdeDebuggerDisposition), alignof(IdeDebuggerDisposition));
    printf("%s;%zu;%zu\n", "IdeDebuggerFrame", sizeof(IdeDebuggerFrame), alignof(IdeDebuggerFrame));
    printf("%s;%zu;%zu\n", "IdeDebuggerFrameClass", sizeof(IdeDebuggerFrameClass), alignof(IdeDebuggerFrameClass));
    printf("%s;%zu;%zu\n", "IdeDebuggerInstruction", sizeof(IdeDebuggerInstruction), alignof(IdeDebuggerInstruction));
    printf("%s;%zu;%zu\n", "IdeDebuggerInstructionClass", sizeof(IdeDebuggerInstructionClass), alignof(IdeDebuggerInstructionClass));
    printf("%s;%zu;%zu\n", "IdeDebuggerLibrary", sizeof(IdeDebuggerLibrary), alignof(IdeDebuggerLibrary));
    printf("%s;%zu;%zu\n", "IdeDebuggerLibraryClass", sizeof(IdeDebuggerLibraryClass), alignof(IdeDebuggerLibraryClass));
    printf("%s;%zu;%zu\n", "IdeDebuggerMovement", sizeof(IdeDebuggerMovement), alignof(IdeDebuggerMovement));
    printf("%s;%zu;%zu\n", "IdeDebuggerRegister", sizeof(IdeDebuggerRegister), alignof(IdeDebuggerRegister));
    printf("%s;%zu;%zu\n", "IdeDebuggerRegisterClass", sizeof(IdeDebuggerRegisterClass), alignof(IdeDebuggerRegisterClass));
    printf("%s;%zu;%zu\n", "IdeDebuggerStopReason", sizeof(IdeDebuggerStopReason), alignof(IdeDebuggerStopReason));
    printf("%s;%zu;%zu\n", "IdeDebuggerStream", sizeof(IdeDebuggerStream), alignof(IdeDebuggerStream));
    printf("%s;%zu;%zu\n", "IdeDebuggerThread", sizeof(IdeDebuggerThread), alignof(IdeDebuggerThread));
    printf("%s;%zu;%zu\n", "IdeDebuggerThreadClass", sizeof(IdeDebuggerThreadClass), alignof(IdeDebuggerThreadClass));
    printf("%s;%zu;%zu\n", "IdeDebuggerThreadGroup", sizeof(IdeDebuggerThreadGroup), alignof(IdeDebuggerThreadGroup));
    printf("%s;%zu;%zu\n", "IdeDebuggerThreadGroupClass", sizeof(IdeDebuggerThreadGroupClass), alignof(IdeDebuggerThreadGroupClass));
    printf("%s;%zu;%zu\n", "IdeDebuggerVariable", sizeof(IdeDebuggerVariable), alignof(IdeDebuggerVariable));
    printf("%s;%zu;%zu\n", "IdeDebuggerVariableClass", sizeof(IdeDebuggerVariableClass), alignof(IdeDebuggerVariableClass));
    printf("%s;%zu;%zu\n", "IdeDependencyUpdaterInterface", sizeof(IdeDependencyUpdaterInterface), alignof(IdeDependencyUpdaterInterface));
    printf("%s;%zu;%zu\n", "IdeDeployStrategy", sizeof(IdeDeployStrategy), alignof(IdeDeployStrategy));
    printf("%s;%zu;%zu\n", "IdeDeployStrategyClass", sizeof(IdeDeployStrategyClass), alignof(IdeDeployStrategyClass));
    printf("%s;%zu;%zu\n", "IdeDevice", sizeof(IdeDevice), alignof(IdeDevice));
    printf("%s;%zu;%zu\n", "IdeDeviceClass", sizeof(IdeDeviceClass), alignof(IdeDeviceClass));
    printf("%s;%zu;%zu\n", "IdeDeviceError", sizeof(IdeDeviceError), alignof(IdeDeviceError));
    printf("%s;%zu;%zu\n", "IdeDeviceInfoClass", sizeof(IdeDeviceInfoClass), alignof(IdeDeviceInfoClass));
    printf("%s;%zu;%zu\n", "IdeDeviceKind", sizeof(IdeDeviceKind), alignof(IdeDeviceKind));
    printf("%s;%zu;%zu\n", "IdeDeviceManagerClass", sizeof(IdeDeviceManagerClass), alignof(IdeDeviceManagerClass));
    printf("%s;%zu;%zu\n", "IdeDeviceProvider", sizeof(IdeDeviceProvider), alignof(IdeDeviceProvider));
    printf("%s;%zu;%zu\n", "IdeDeviceProviderClass", sizeof(IdeDeviceProviderClass), alignof(IdeDeviceProviderClass));
    printf("%s;%zu;%zu\n", "IdeDiagnostic", sizeof(IdeDiagnostic), alignof(IdeDiagnostic));
    printf("%s;%zu;%zu\n", "IdeDiagnosticClass", sizeof(IdeDiagnosticClass), alignof(IdeDiagnosticClass));
    printf("%s;%zu;%zu\n", "IdeDiagnosticProviderInterface", sizeof(IdeDiagnosticProviderInterface), alignof(IdeDiagnosticProviderInterface));
    printf("%s;%zu;%zu\n", "IdeDiagnosticSeverity", sizeof(IdeDiagnosticSeverity), alignof(IdeDiagnosticSeverity));
    printf("%s;%zu;%zu\n", "IdeDiagnosticTool", sizeof(IdeDiagnosticTool), alignof(IdeDiagnosticTool));
    printf("%s;%zu;%zu\n", "IdeDiagnosticToolClass", sizeof(IdeDiagnosticToolClass), alignof(IdeDiagnosticToolClass));
    printf("%s;%zu;%zu\n", "IdeDiagnostics", sizeof(IdeDiagnostics), alignof(IdeDiagnostics));
    printf("%s;%zu;%zu\n", "IdeDiagnosticsClass", sizeof(IdeDiagnosticsClass), alignof(IdeDiagnosticsClass));
    printf("%s;%zu;%zu\n", "IdeDiagnosticsManagerClass", sizeof(IdeDiagnosticsManagerClass), alignof(IdeDiagnosticsManagerClass));
    printf("%s;%zu;%zu\n", "IdeDirectoryReaperClass", sizeof(IdeDirectoryReaperClass), alignof(IdeDirectoryReaperClass));
    printf("%s;%zu;%zu\n", "IdeDirectoryVcsClass", sizeof(IdeDirectoryVcsClass), alignof(IdeDirectoryVcsClass));
    printf("%s;%zu;%zu\n", "IdeDoapClass", sizeof(IdeDoapClass), alignof(IdeDoapClass));
    printf("%s;%zu;%zu\n", "IdeDoapError", sizeof(IdeDoapError), alignof(IdeDoapError));
    printf("%s;%zu;%zu\n", "IdeDoapPersonClass", sizeof(IdeDoapPersonClass), alignof(IdeDoapPersonClass));
    printf("%s;%zu;%zu\n", "IdeEditorPageAddinInterface", sizeof(IdeEditorPageAddinInterface), alignof(IdeEditorPageAddinInterface));
    printf("%s;%zu;%zu\n", "IdeEditorPageClass", sizeof(IdeEditorPageClass), alignof(IdeEditorPageClass));
    printf("%s;%zu;%zu\n", "IdeEntryPopover", sizeof(IdeEntryPopover), alignof(IdeEntryPopover));
    printf("%s;%zu;%zu\n", "IdeEntryPopoverClass", sizeof(IdeEntryPopoverClass), alignof(IdeEntryPopoverClass));
    printf("%s;%zu;%zu\n", "IdeEnumObjectClass", sizeof(IdeEnumObjectClass), alignof(IdeEnumObjectClass));
    printf("%s;%zu;%zu\n", "IdeEnvironmentClass", sizeof(IdeEnvironmentClass), alignof(IdeEnvironmentClass));
    printf("%s;%zu;%zu\n", "IdeEnvironmentEditorClass", sizeof(IdeEnvironmentEditorClass), alignof(IdeEnvironmentEditorClass));
    printf("%s;%zu;%zu\n", "IdeEnvironmentVariableClass", sizeof(IdeEnvironmentVariableClass), alignof(IdeEnvironmentVariableClass));
    printf("%s;%zu;%zu\n", "IdeExtensionAdapterClass", sizeof(IdeExtensionAdapterClass), alignof(IdeExtensionAdapterClass));
    printf("%s;%zu;%zu\n", "IdeExtensionSetAdapterClass", sizeof(IdeExtensionSetAdapterClass), alignof(IdeExtensionSetAdapterClass));
    printf("%s;%zu;%zu\n", "IdeFallbackBuildSystemClass", sizeof(IdeFallbackBuildSystemClass), alignof(IdeFallbackBuildSystemClass));
    printf("%s;%zu;%zu\n", "IdeFancyTreeView", sizeof(IdeFancyTreeView), alignof(IdeFancyTreeView));
    printf("%s;%zu;%zu\n", "IdeFancyTreeViewClass", sizeof(IdeFancyTreeViewClass), alignof(IdeFancyTreeViewClass));
    printf("%s;%zu;%zu\n", "IdeFileChooserEntryClass", sizeof(IdeFileChooserEntryClass), alignof(IdeFileChooserEntryClass));
    printf("%s;%zu;%zu\n", "IdeFileSettings", sizeof(IdeFileSettings), alignof(IdeFileSettings));
    printf("%s;%zu;%zu\n", "IdeFileSettingsClass", sizeof(IdeFileSettingsClass), alignof(IdeFileSettingsClass));
    printf("%s;%zu;%zu\n", "IdeFileTransfer", sizeof(IdeFileTransfer), alignof(IdeFileTransfer));
    printf("%s;%zu;%zu\n", "IdeFileTransferClass", sizeof(IdeFileTransferClass), alignof(IdeFileTransferClass));
    printf("%s;%zu;%zu\n", "IdeFileTransferFlags", sizeof(IdeFileTransferFlags), alignof(IdeFileTransferFlags));
    printf("%s;%zu;%zu\n", "IdeFileTransferStat", sizeof(IdeFileTransferStat), alignof(IdeFileTransferStat));
    printf("%s;%zu;%zu\n", "IdeFormatterInterface", sizeof(IdeFormatterInterface), alignof(IdeFormatterInterface));
    printf("%s;%zu;%zu\n", "IdeFormatterOptionsClass", sizeof(IdeFormatterOptionsClass), alignof(IdeFormatterOptionsClass));
    printf("%s;%zu;%zu\n", "IdeFrameAddinInterface", sizeof(IdeFrameAddinInterface), alignof(IdeFrameAddinInterface));
    printf("%s;%zu;%zu\n", "IdeFrameClass", sizeof(IdeFrameClass), alignof(IdeFrameClass));
    printf("%s;%zu;%zu\n", "IdeFuzzyIndexBuilderClass", sizeof(IdeFuzzyIndexBuilderClass), alignof(IdeFuzzyIndexBuilderClass));
    printf("%s;%zu;%zu\n", "IdeFuzzyIndexClass", sizeof(IdeFuzzyIndexClass), alignof(IdeFuzzyIndexClass));
    printf("%s;%zu;%zu\n", "IdeFuzzyIndexCursorClass", sizeof(IdeFuzzyIndexCursorClass), alignof(IdeFuzzyIndexCursorClass));
    printf("%s;%zu;%zu\n", "IdeFuzzyIndexMatchClass", sizeof(IdeFuzzyIndexMatchClass), alignof(IdeFuzzyIndexMatchClass));
    printf("%s;%zu;%zu\n", "IdeFuzzyMutableIndexMatch", sizeof(IdeFuzzyMutableIndexMatch), alignof(IdeFuzzyMutableIndexMatch));
    printf("%s;%zu;%zu\n", "IdeGreeterRow", sizeof(IdeGreeterRow), alignof(IdeGreeterRow));
    printf("%s;%zu;%zu\n", "IdeGreeterRowClass", sizeof(IdeGreeterRowClass), alignof(IdeGreeterRowClass));
    printf("%s;%zu;%zu\n", "IdeGreeterSectionInterface", sizeof(IdeGreeterSectionInterface), alignof(IdeGreeterSectionInterface));
    printf("%s;%zu;%zu\n", "IdeGridClass", sizeof(IdeGridClass), alignof(IdeGridClass));
    printf("%s;%zu;%zu\n", "IdeGutterInterface", sizeof(IdeGutterInterface), alignof(IdeGutterInterface));
    printf("%s;%zu;%zu\n", "IdeHeaderBar", sizeof(IdeHeaderBar), alignof(IdeHeaderBar));
    printf("%s;%zu;%zu\n", "IdeHeaderBarClass", sizeof(IdeHeaderBarClass), alignof(IdeHeaderBarClass));
    printf("%s;%zu;%zu\n", "IdeHeaderBarPosition", sizeof(IdeHeaderBarPosition), alignof(IdeHeaderBarPosition));
    printf("%s;%zu;%zu\n", "IdeHeap", sizeof(IdeHeap), alignof(IdeHeap));
    printf("%s;%zu;%zu\n", "IdeHighlightEngineClass", sizeof(IdeHighlightEngineClass), alignof(IdeHighlightEngineClass));
    printf("%s;%zu;%zu\n", "IdeHighlightResult", sizeof(IdeHighlightResult), alignof(IdeHighlightResult));
    printf("%s;%zu;%zu\n", "IdeHighlighterInterface", sizeof(IdeHighlighterInterface), alignof(IdeHighlighterInterface));
    printf("%s;%zu;%zu\n", "IdeHtmlGenerator", sizeof(IdeHtmlGenerator), alignof(IdeHtmlGenerator));
    printf("%s;%zu;%zu\n", "IdeHtmlGeneratorClass", sizeof(IdeHtmlGeneratorClass), alignof(IdeHtmlGeneratorClass));
    printf("%s;%zu;%zu\n", "IdeIndentStyle", sizeof(IdeIndentStyle), alignof(IdeIndentStyle));
    printf("%s;%zu;%zu\n", "IdeJoinedMenuClass", sizeof(IdeJoinedMenuClass), alignof(IdeJoinedMenuClass));
    printf("%s;%zu;%zu\n", "IdeLineChangeGutterRendererClass", sizeof(IdeLineChangeGutterRendererClass), alignof(IdeLineChangeGutterRendererClass));
    printf("%s;%zu;%zu\n", "IdeLineReader", sizeof(IdeLineReader), alignof(IdeLineReader));
    printf("%s;%zu;%zu\n", "IdeLocalDevice", sizeof(IdeLocalDevice), alignof(IdeLocalDevice));
    printf("%s;%zu;%zu\n", "IdeLocalDeviceClass", sizeof(IdeLocalDeviceClass), alignof(IdeLocalDeviceClass));
    printf("%s;%zu;%zu\n", "IdeLocation", sizeof(IdeLocation), alignof(IdeLocation));
    printf("%s;%zu;%zu\n", "IdeLocationClass", sizeof(IdeLocationClass), alignof(IdeLocationClass));
    printf("%s;%zu;%zu\n", "IdeLspClient", sizeof(IdeLspClient), alignof(IdeLspClient));
    printf("%s;%zu;%zu\n", "IdeLspClientClass", sizeof(IdeLspClientClass), alignof(IdeLspClientClass));
    printf("%s;%zu;%zu\n", "IdeLspCodeAction", sizeof(IdeLspCodeAction), alignof(IdeLspCodeAction));
    printf("%s;%zu;%zu\n", "IdeLspCodeActionClass", sizeof(IdeLspCodeActionClass), alignof(IdeLspCodeActionClass));
    printf("%s;%zu;%zu\n", "IdeLspCodeActionProvider", sizeof(IdeLspCodeActionProvider), alignof(IdeLspCodeActionProvider));
    printf("%s;%zu;%zu\n", "IdeLspCodeActionProviderClass", sizeof(IdeLspCodeActionProviderClass), alignof(IdeLspCodeActionProviderClass));
    printf("%s;%zu;%zu\n", "IdeLspCompletionItemClass", sizeof(IdeLspCompletionItemClass), alignof(IdeLspCompletionItemClass));
    printf("%s;%zu;%zu\n", "IdeLspCompletionKind", sizeof(IdeLspCompletionKind), alignof(IdeLspCompletionKind));
    printf("%s;%zu;%zu\n", "IdeLspCompletionProvider", sizeof(IdeLspCompletionProvider), alignof(IdeLspCompletionProvider));
    printf("%s;%zu;%zu\n", "IdeLspCompletionProviderClass", sizeof(IdeLspCompletionProviderClass), alignof(IdeLspCompletionProviderClass));
    printf("%s;%zu;%zu\n", "IdeLspCompletionResultsClass", sizeof(IdeLspCompletionResultsClass), alignof(IdeLspCompletionResultsClass));
    printf("%s;%zu;%zu\n", "IdeLspDiagnostic", sizeof(IdeLspDiagnostic), alignof(IdeLspDiagnostic));
    printf("%s;%zu;%zu\n", "IdeLspDiagnosticClass", sizeof(IdeLspDiagnosticClass), alignof(IdeLspDiagnosticClass));
    printf("%s;%zu;%zu\n", "IdeLspDiagnosticProvider", sizeof(IdeLspDiagnosticProvider), alignof(IdeLspDiagnosticProvider));
    printf("%s;%zu;%zu\n", "IdeLspDiagnosticProviderClass", sizeof(IdeLspDiagnosticProviderClass), alignof(IdeLspDiagnosticProviderClass));
    printf("%s;%zu;%zu\n", "IdeLspFormatter", sizeof(IdeLspFormatter), alignof(IdeLspFormatter));
    printf("%s;%zu;%zu\n", "IdeLspFormatterClass", sizeof(IdeLspFormatterClass), alignof(IdeLspFormatterClass));
    printf("%s;%zu;%zu\n", "IdeLspHighlighter", sizeof(IdeLspHighlighter), alignof(IdeLspHighlighter));
    printf("%s;%zu;%zu\n", "IdeLspHighlighterClass", sizeof(IdeLspHighlighterClass), alignof(IdeLspHighlighterClass));
    printf("%s;%zu;%zu\n", "IdeLspHoverProvider", sizeof(IdeLspHoverProvider), alignof(IdeLspHoverProvider));
    printf("%s;%zu;%zu\n", "IdeLspHoverProviderClass", sizeof(IdeLspHoverProviderClass), alignof(IdeLspHoverProviderClass));
    printf("%s;%zu;%zu\n", "IdeLspRenameProvider", sizeof(IdeLspRenameProvider), alignof(IdeLspRenameProvider));
    printf("%s;%zu;%zu\n", "IdeLspRenameProviderClass", sizeof(IdeLspRenameProviderClass), alignof(IdeLspRenameProviderClass));
    printf("%s;%zu;%zu\n", "IdeLspSearchProvider", sizeof(IdeLspSearchProvider), alignof(IdeLspSearchProvider));
    printf("%s;%zu;%zu\n", "IdeLspSearchProviderClass", sizeof(IdeLspSearchProviderClass), alignof(IdeLspSearchProviderClass));
    printf("%s;%zu;%zu\n", "IdeLspService", sizeof(IdeLspService), alignof(IdeLspService));
    printf("%s;%zu;%zu\n", "IdeLspServiceClass", sizeof(IdeLspServiceClass), alignof(IdeLspServiceClass));
    printf("%s;%zu;%zu\n", "IdeLspSymbolNodeClass", sizeof(IdeLspSymbolNodeClass), alignof(IdeLspSymbolNodeClass));
    printf("%s;%zu;%zu\n", "IdeLspSymbolResolver", sizeof(IdeLspSymbolResolver), alignof(IdeLspSymbolResolver));
    printf("%s;%zu;%zu\n", "IdeLspSymbolResolverClass", sizeof(IdeLspSymbolResolverClass), alignof(IdeLspSymbolResolverClass));
    printf("%s;%zu;%zu\n", "IdeLspSymbolTreeClass", sizeof(IdeLspSymbolTreeClass), alignof(IdeLspSymbolTreeClass));
    printf("%s;%zu;%zu\n", "IdeLspTrace", sizeof(IdeLspTrace), alignof(IdeLspTrace));
    printf("%s;%zu;%zu\n", "IdeLspWorkspaceEditClass", sizeof(IdeLspWorkspaceEditClass), alignof(IdeLspWorkspaceEditClass));
    printf("%s;%zu;%zu\n", "IdeMarkedKind", sizeof(IdeMarkedKind), alignof(IdeMarkedKind));
    printf("%s;%zu;%zu\n", "IdeMarkedViewClass", sizeof(IdeMarkedViewClass), alignof(IdeMarkedViewClass));
    printf("%s;%zu;%zu\n", "IdeMenuManagerClass", sizeof(IdeMenuManagerClass), alignof(IdeMenuManagerClass));
    printf("%s;%zu;%zu\n", "IdeNotification", sizeof(IdeNotification), alignof(IdeNotification));
    printf("%s;%zu;%zu\n", "IdeNotificationClass", sizeof(IdeNotificationClass), alignof(IdeNotificationClass));
    printf("%s;%zu;%zu\n", "IdeNotificationsButtonClass", sizeof(IdeNotificationsButtonClass), alignof(IdeNotificationsButtonClass));
    printf("%s;%zu;%zu\n", "IdeNotificationsClass", sizeof(IdeNotificationsClass), alignof(IdeNotificationsClass));
    printf("%s;%zu;%zu\n", "IdeObject", sizeof(IdeObject), alignof(IdeObject));
    printf("%s;%zu;%zu\n", "IdeObjectBoxClass", sizeof(IdeObjectBoxClass), alignof(IdeObjectBoxClass));
    printf("%s;%zu;%zu\n", "IdeObjectClass", sizeof(IdeObjectClass), alignof(IdeObjectClass));
    printf("%s;%zu;%zu\n", "IdeObjectLocation", sizeof(IdeObjectLocation), alignof(IdeObjectLocation));
    printf("%s;%zu;%zu\n", "IdeOmniBarAddinInterface", sizeof(IdeOmniBarAddinInterface), alignof(IdeOmniBarAddinInterface));
    printf("%s;%zu;%zu\n", "IdeOmniBarClass", sizeof(IdeOmniBarClass), alignof(IdeOmniBarClass));
    printf("%s;%zu;%zu\n", "IdePage", sizeof(IdePage), alignof(IdePage));
    printf("%s;%zu;%zu\n", "IdePageClass", sizeof(IdePageClass), alignof(IdePageClass));
    printf("%s;%zu;%zu\n", "IdePane", sizeof(IdePane), alignof(IdePane));
    printf("%s;%zu;%zu\n", "IdePaneClass", sizeof(IdePaneClass), alignof(IdePaneClass));
    printf("%s;%zu;%zu\n", "IdePersistentMapBuilderClass", sizeof(IdePersistentMapBuilderClass), alignof(IdePersistentMapBuilderClass));
    printf("%s;%zu;%zu\n", "IdePersistentMapClass", sizeof(IdePersistentMapClass), alignof(IdePersistentMapClass));
    printf("%s;%zu;%zu\n", "IdePipelineAddinInterface", sizeof(IdePipelineAddinInterface), alignof(IdePipelineAddinInterface));
    printf("%s;%zu;%zu\n", "IdePipelineClass", sizeof(IdePipelineClass), alignof(IdePipelineClass));
    printf("%s;%zu;%zu\n", "IdePipelinePhase", sizeof(IdePipelinePhase), alignof(IdePipelinePhase));
    printf("%s;%zu;%zu\n", "IdePipelineStage", sizeof(IdePipelineStage), alignof(IdePipelineStage));
    printf("%s;%zu;%zu\n", "IdePipelineStageClass", sizeof(IdePipelineStageClass), alignof(IdePipelineStageClass));
    printf("%s;%zu;%zu\n", "IdePipelineStageLauncher", sizeof(IdePipelineStageLauncher), alignof(IdePipelineStageLauncher));
    printf("%s;%zu;%zu\n", "IdePipelineStageLauncherClass", sizeof(IdePipelineStageLauncherClass), alignof(IdePipelineStageLauncherClass));
    printf("%s;%zu;%zu\n", "IdePipelineStageMkdirs", sizeof(IdePipelineStageMkdirs), alignof(IdePipelineStageMkdirs));
    printf("%s;%zu;%zu\n", "IdePipelineStageMkdirsClass", sizeof(IdePipelineStageMkdirsClass), alignof(IdePipelineStageMkdirsClass));
    printf("%s;%zu;%zu\n", "IdePipelineStageTransferClass", sizeof(IdePipelineStageTransferClass), alignof(IdePipelineStageTransferClass));
    printf("%s;%zu;%zu\n", "IdePkconTransferClass", sizeof(IdePkconTransferClass), alignof(IdePkconTransferClass));
    printf("%s;%zu;%zu\n", "IdePopoverPositionerInterface", sizeof(IdePopoverPositionerInterface), alignof(IdePopoverPositionerInterface));
    printf("%s;%zu;%zu\n", "IdePreferenceGroupEntry", sizeof(IdePreferenceGroupEntry), alignof(IdePreferenceGroupEntry));
    printf("%s;%zu;%zu\n", "IdePreferenceItemEntry", sizeof(IdePreferenceItemEntry), alignof(IdePreferenceItemEntry));
    printf("%s;%zu;%zu\n", "IdePreferencePageEntry", sizeof(IdePreferencePageEntry), alignof(IdePreferencePageEntry));
    printf("%s;%zu;%zu\n", "IdePreferencesAddinInterface", sizeof(IdePreferencesAddinInterface), alignof(IdePreferencesAddinInterface));
    printf("%s;%zu;%zu\n", "IdePreferencesChoiceRowClass", sizeof(IdePreferencesChoiceRowClass), alignof(IdePreferencesChoiceRowClass));
    printf("%s;%zu;%zu\n", "IdePreferencesMode", sizeof(IdePreferencesMode), alignof(IdePreferencesMode));
    printf("%s;%zu;%zu\n", "IdePreferencesWindowClass", sizeof(IdePreferencesWindowClass), alignof(IdePreferencesWindowClass));
    printf("%s;%zu;%zu\n", "IdeProcessKind", sizeof(IdeProcessKind), alignof(IdeProcessKind));
    printf("%s;%zu;%zu\n", "IdeProgressIconClass", sizeof(IdeProgressIconClass), alignof(IdeProgressIconClass));
    printf("%s;%zu;%zu\n", "IdeProjectClass", sizeof(IdeProjectClass), alignof(IdeProjectClass));
    printf("%s;%zu;%zu\n", "IdeProjectFile", sizeof(IdeProjectFile), alignof(IdeProjectFile));
    printf("%s;%zu;%zu\n", "IdeProjectFileClass", sizeof(IdeProjectFileClass), alignof(IdeProjectFileClass));
    printf("%s;%zu;%zu\n", "IdeProjectInfoClass", sizeof(IdeProjectInfoClass), alignof(IdeProjectInfoClass));
    printf("%s;%zu;%zu\n", "IdeProjectTemplate", sizeof(IdeProjectTemplate), alignof(IdeProjectTemplate));
    printf("%s;%zu;%zu\n", "IdeProjectTemplateClass", sizeof(IdeProjectTemplateClass), alignof(IdeProjectTemplateClass));
    printf("%s;%zu;%zu\n", "IdeProjectTreeAddinInterface", sizeof(IdeProjectTreeAddinInterface), alignof(IdeProjectTreeAddinInterface));
    printf("%s;%zu;%zu\n", "IdePtyFd", sizeof(IdePtyFd), alignof(IdePtyFd));
    printf("%s;%zu;%zu\n", "IdePtyIntercept", sizeof(IdePtyIntercept), alignof(IdePtyIntercept));
    printf("%s;%zu;%zu\n", "IdePtyInterceptSide", sizeof(IdePtyInterceptSide), alignof(IdePtyInterceptSide));
    printf("%s;%zu;%zu\n", "IdeRadioBoxClass", sizeof(IdeRadioBoxClass), alignof(IdeRadioBoxClass));
    printf("%s;%zu;%zu\n", "IdeRange", sizeof(IdeRange), alignof(IdeRange));
    printf("%s;%zu;%zu\n", "IdeRangeClass", sizeof(IdeRangeClass), alignof(IdeRangeClass));
    printf("%s;%zu;%zu\n", "IdeRecentProjectsClass", sizeof(IdeRecentProjectsClass), alignof(IdeRecentProjectsClass));
    printf("%s;%zu;%zu\n", "IdeRecursiveFileMonitorClass", sizeof(IdeRecursiveFileMonitorClass), alignof(IdeRecursiveFileMonitorClass));
    printf("%s;%zu;%zu\n", "IdeRenameProviderInterface", sizeof(IdeRenameProviderInterface), alignof(IdeRenameProviderInterface));
    printf("%s;%zu;%zu\n", "IdeRunButtonClass", sizeof(IdeRunButtonClass), alignof(IdeRunButtonClass));
    printf("%s;%zu;%zu\n", "IdeRunCommand", sizeof(IdeRunCommand), alignof(IdeRunCommand));
    printf("%s;%zu;%zu\n", "IdeRunCommandClass", sizeof(IdeRunCommandClass), alignof(IdeRunCommandClass));
    printf("%s;%zu;%zu\n", "IdeRunCommandKind", sizeof(IdeRunCommandKind), alignof(IdeRunCommandKind));
    printf("%s;%zu;%zu\n", "IdeRunCommandProviderInterface", sizeof(IdeRunCommandProviderInterface), alignof(IdeRunCommandProviderInterface));
    printf("%s;%zu;%zu\n", "IdeRunManagerClass", sizeof(IdeRunManagerClass), alignof(IdeRunManagerClass));
    printf("%s;%zu;%zu\n", "IdeRunner", sizeof(IdeRunner), alignof(IdeRunner));
    printf("%s;%zu;%zu\n", "IdeRunnerAddinInterface", sizeof(IdeRunnerAddinInterface), alignof(IdeRunnerAddinInterface));
    printf("%s;%zu;%zu\n", "IdeRunnerClass", sizeof(IdeRunnerClass), alignof(IdeRunnerClass));
    printf("%s;%zu;%zu\n", "IdeRuntime", sizeof(IdeRuntime), alignof(IdeRuntime));
    printf("%s;%zu;%zu\n", "IdeRuntimeClass", sizeof(IdeRuntimeClass), alignof(IdeRuntimeClass));
    printf("%s;%zu;%zu\n", "IdeRuntimeError", sizeof(IdeRuntimeError), alignof(IdeRuntimeError));
    printf("%s;%zu;%zu\n", "IdeRuntimeManagerClass", sizeof(IdeRuntimeManagerClass), alignof(IdeRuntimeManagerClass));
    printf("%s;%zu;%zu\n", "IdeRuntimeProviderInterface", sizeof(IdeRuntimeProviderInterface), alignof(IdeRuntimeProviderInterface));
    printf("%s;%zu;%zu\n", "IdeSearchEngineClass", sizeof(IdeSearchEngineClass), alignof(IdeSearchEngineClass));
    printf("%s;%zu;%zu\n", "IdeSearchEntryClass", sizeof(IdeSearchEntryClass), alignof(IdeSearchEntryClass));
    printf("%s;%zu;%zu\n", "IdeSearchPopoverClass", sizeof(IdeSearchPopoverClass), alignof(IdeSearchPopoverClass));
    printf("%s;%zu;%zu\n", "IdeSearchProviderInterface", sizeof(IdeSearchProviderInterface), alignof(IdeSearchProviderInterface));
    printf("%s;%zu;%zu\n", "IdeSearchReducer", sizeof(IdeSearchReducer), alignof(IdeSearchReducer));
    printf("%s;%zu;%zu\n", "IdeSearchResult", sizeof(IdeSearchResult), alignof(IdeSearchResult));
    printf("%s;%zu;%zu\n", "IdeSearchResultClass", sizeof(IdeSearchResultClass), alignof(IdeSearchResultClass));
    printf("%s;%zu;%zu\n", "IdeSessionAddinInterface", sizeof(IdeSessionAddinInterface), alignof(IdeSessionAddinInterface));
    printf("%s;%zu;%zu\n", "IdeSettingsClass", sizeof(IdeSettingsClass), alignof(IdeSettingsClass));
    printf("%s;%zu;%zu\n", "IdeSignalGroupClass", sizeof(IdeSignalGroupClass), alignof(IdeSignalGroupClass));
    printf("%s;%zu;%zu\n", "IdeSimilarFileLocatorInterface", sizeof(IdeSimilarFileLocatorInterface), alignof(IdeSimilarFileLocatorInterface));
    printf("%s;%zu;%zu\n", "IdeSimpleBuildSystemDiscovery", sizeof(IdeSimpleBuildSystemDiscovery), alignof(IdeSimpleBuildSystemDiscovery));
    printf("%s;%zu;%zu\n", "IdeSimpleBuildSystemDiscoveryClass", sizeof(IdeSimpleBuildSystemDiscoveryClass), alignof(IdeSimpleBuildSystemDiscoveryClass));
    printf("%s;%zu;%zu\n", "IdeSimpleBuildTarget", sizeof(IdeSimpleBuildTarget), alignof(IdeSimpleBuildTarget));
    printf("%s;%zu;%zu\n", "IdeSimpleBuildTargetClass", sizeof(IdeSimpleBuildTargetClass), alignof(IdeSimpleBuildTargetClass));
    printf("%s;%zu;%zu\n", "IdeSimpleToolchain", sizeof(IdeSimpleToolchain), alignof(IdeSimpleToolchain));
    printf("%s;%zu;%zu\n", "IdeSimpleToolchainClass", sizeof(IdeSimpleToolchainClass), alignof(IdeSimpleToolchainClass));
    printf("%s;%zu;%zu\n", "IdeSourceViewClass", sizeof(IdeSourceViewClass), alignof(IdeSourceViewClass));
    printf("%s;%zu;%zu\n", "IdeSpacesStyle", sizeof(IdeSpacesStyle), alignof(IdeSpacesStyle));
    printf("%s;%zu;%zu\n", "IdeSubprocessInterface", sizeof(IdeSubprocessInterface), alignof(IdeSubprocessInterface));
    printf("%s;%zu;%zu\n", "IdeSubprocessLauncher", sizeof(IdeSubprocessLauncher), alignof(IdeSubprocessLauncher));
    printf("%s;%zu;%zu\n", "IdeSubprocessLauncherClass", sizeof(IdeSubprocessLauncherClass), alignof(IdeSubprocessLauncherClass));
    printf("%s;%zu;%zu\n", "IdeSubprocessSupervisor", sizeof(IdeSubprocessSupervisor), alignof(IdeSubprocessSupervisor));
    printf("%s;%zu;%zu\n", "IdeSubprocessSupervisorClass", sizeof(IdeSubprocessSupervisorClass), alignof(IdeSubprocessSupervisorClass));
    printf("%s;%zu;%zu\n", "IdeSymbol", sizeof(IdeSymbol), alignof(IdeSymbol));
    printf("%s;%zu;%zu\n", "IdeSymbolClass", sizeof(IdeSymbolClass), alignof(IdeSymbolClass));
    printf("%s;%zu;%zu\n", "IdeSymbolFlags", sizeof(IdeSymbolFlags), alignof(IdeSymbolFlags));
    printf("%s;%zu;%zu\n", "IdeSymbolKind", sizeof(IdeSymbolKind), alignof(IdeSymbolKind));
    printf("%s;%zu;%zu\n", "IdeSymbolNode", sizeof(IdeSymbolNode), alignof(IdeSymbolNode));
    printf("%s;%zu;%zu\n", "IdeSymbolNodeClass", sizeof(IdeSymbolNodeClass), alignof(IdeSymbolNodeClass));
    printf("%s;%zu;%zu\n", "IdeSymbolResolverInterface", sizeof(IdeSymbolResolverInterface), alignof(IdeSymbolResolverInterface));
    printf("%s;%zu;%zu\n", "IdeSymbolTreeInterface", sizeof(IdeSymbolTreeInterface), alignof(IdeSymbolTreeInterface));
    printf("%s;%zu;%zu\n", "IdeTaskCacheClass", sizeof(IdeTaskCacheClass), alignof(IdeTaskCacheClass));
    printf("%s;%zu;%zu\n", "IdeTaskClass", sizeof(IdeTaskClass), alignof(IdeTaskClass));
    printf("%s;%zu;%zu\n", "IdeTaskKind", sizeof(IdeTaskKind), alignof(IdeTaskKind));
    printf("%s;%zu;%zu\n", "IdeTemplateBase", sizeof(IdeTemplateBase), alignof(IdeTemplateBase));
    printf("%s;%zu;%zu\n", "IdeTemplateBaseClass", sizeof(IdeTemplateBaseClass), alignof(IdeTemplateBaseClass));
    printf("%s;%zu;%zu\n", "IdeTemplateInputClass", sizeof(IdeTemplateInputClass), alignof(IdeTemplateInputClass));
    printf("%s;%zu;%zu\n", "IdeTemplateInputValidation", sizeof(IdeTemplateInputValidation), alignof(IdeTemplateInputValidation));
    printf("%s;%zu;%zu\n", "IdeTemplateLocator", sizeof(IdeTemplateLocator), alignof(IdeTemplateLocator));
    printf("%s;%zu;%zu\n", "IdeTemplateLocatorClass", sizeof(IdeTemplateLocatorClass), alignof(IdeTemplateLocatorClass));
    printf("%s;%zu;%zu\n", "IdeTemplateProviderInterface", sizeof(IdeTemplateProviderInterface), alignof(IdeTemplateProviderInterface));
    printf("%s;%zu;%zu\n", "IdeTerminal", sizeof(IdeTerminal), alignof(IdeTerminal));
    printf("%s;%zu;%zu\n", "IdeTerminalClass", sizeof(IdeTerminalClass), alignof(IdeTerminalClass));
    printf("%s;%zu;%zu\n", "IdeTerminalLauncherClass", sizeof(IdeTerminalLauncherClass), alignof(IdeTerminalLauncherClass));
    printf("%s;%zu;%zu\n", "IdeTerminalPageClass", sizeof(IdeTerminalPageClass), alignof(IdeTerminalPageClass));
    printf("%s;%zu;%zu\n", "IdeTerminalPopoverClass", sizeof(IdeTerminalPopoverClass), alignof(IdeTerminalPopoverClass));
    printf("%s;%zu;%zu\n", "IdeTerminalSearchClass", sizeof(IdeTerminalSearchClass), alignof(IdeTerminalSearchClass));
    printf("%s;%zu;%zu\n", "IdeTest", sizeof(IdeTest), alignof(IdeTest));
    printf("%s;%zu;%zu\n", "IdeTestClass", sizeof(IdeTestClass), alignof(IdeTestClass));
    printf("%s;%zu;%zu\n", "IdeTestManagerClass", sizeof(IdeTestManagerClass), alignof(IdeTestManagerClass));
    printf("%s;%zu;%zu\n", "IdeTestProvider", sizeof(IdeTestProvider), alignof(IdeTestProvider));
    printf("%s;%zu;%zu\n", "IdeTestProviderClass", sizeof(IdeTestProviderClass), alignof(IdeTestProviderClass));
    printf("%s;%zu;%zu\n", "IdeTestStatus", sizeof(IdeTestStatus), alignof(IdeTestStatus));
    printf("%s;%zu;%zu\n", "IdeTextEdit", sizeof(IdeTextEdit), alignof(IdeTextEdit));
    printf("%s;%zu;%zu\n", "IdeTextEditClass", sizeof(IdeTextEditClass), alignof(IdeTextEditClass));
    printf("%s;%zu;%zu\n", "IdeThreadPoolKind", sizeof(IdeThreadPoolKind), alignof(IdeThreadPoolKind));
    printf("%s;%zu;%zu\n", "IdeThreeGridClass", sizeof(IdeThreeGridClass), alignof(IdeThreeGridClass));
    printf("%s;%zu;%zu\n", "IdeThreeGridColumn", sizeof(IdeThreeGridColumn), alignof(IdeThreeGridColumn));
    printf("%s;%zu;%zu\n", "IdeToolchain", sizeof(IdeToolchain), alignof(IdeToolchain));
    printf("%s;%zu;%zu\n", "IdeToolchainClass", sizeof(IdeToolchainClass), alignof(IdeToolchainClass));
    printf("%s;%zu;%zu\n", "IdeToolchainManagerClass", sizeof(IdeToolchainManagerClass), alignof(IdeToolchainManagerClass));
    printf("%s;%zu;%zu\n", "IdeToolchainProviderInterface", sizeof(IdeToolchainProviderInterface), alignof(IdeToolchainProviderInterface));
    printf("%s;%zu;%zu\n", "IdeTransfer", sizeof(IdeTransfer), alignof(IdeTransfer));
    printf("%s;%zu;%zu\n", "IdeTransferClass", sizeof(IdeTransferClass), alignof(IdeTransferClass));
    printf("%s;%zu;%zu\n", "IdeTransferError", sizeof(IdeTransferError), alignof(IdeTransferError));
    printf("%s;%zu;%zu\n", "IdeTransferManagerClass", sizeof(IdeTransferManagerClass), alignof(IdeTransferManagerClass));
    printf("%s;%zu;%zu\n", "IdeTree", sizeof(IdeTree), alignof(IdeTree));
    printf("%s;%zu;%zu\n", "IdeTreeAddinInterface", sizeof(IdeTreeAddinInterface), alignof(IdeTreeAddinInterface));
    printf("%s;%zu;%zu\n", "IdeTreeClass", sizeof(IdeTreeClass), alignof(IdeTreeClass));
    printf("%s;%zu;%zu\n", "IdeTreeModelClass", sizeof(IdeTreeModelClass), alignof(IdeTreeModelClass));
    printf("%s;%zu;%zu\n", "IdeTreeNodeClass", sizeof(IdeTreeNodeClass), alignof(IdeTreeNodeClass));
    printf("%s;%zu;%zu\n", "IdeTreeNodeFlags", sizeof(IdeTreeNodeFlags), alignof(IdeTreeNodeFlags));
    printf("%s;%zu;%zu\n", "IdeTreeNodeVisit", sizeof(IdeTreeNodeVisit), alignof(IdeTreeNodeVisit));
    printf("%s;%zu;%zu\n", "IdeTruncateModelClass", sizeof(IdeTruncateModelClass), alignof(IdeTruncateModelClass));
    printf("%s;%zu;%zu\n", "IdeUnsavedFilesClass", sizeof(IdeUnsavedFilesClass), alignof(IdeUnsavedFilesClass));
    printf("%s;%zu;%zu\n", "IdeVcsBranchInterface", sizeof(IdeVcsBranchInterface), alignof(IdeVcsBranchInterface));
    printf("%s;%zu;%zu\n", "IdeVcsCloneRequestClass", sizeof(IdeVcsCloneRequestClass), alignof(IdeVcsCloneRequestClass));
    printf("%s;%zu;%zu\n", "IdeVcsCloneRequestValidation", sizeof(IdeVcsCloneRequestValidation), alignof(IdeVcsCloneRequestValidation));
    printf("%s;%zu;%zu\n", "IdeVcsClonerInterface", sizeof(IdeVcsClonerInterface), alignof(IdeVcsClonerInterface));
    printf("%s;%zu;%zu\n", "IdeVcsConfigInterface", sizeof(IdeVcsConfigInterface), alignof(IdeVcsConfigInterface));
    printf("%s;%zu;%zu\n", "IdeVcsConfigType", sizeof(IdeVcsConfigType), alignof(IdeVcsConfigType));
    printf("%s;%zu;%zu\n", "IdeVcsFileInfo", sizeof(IdeVcsFileInfo), alignof(IdeVcsFileInfo));
    printf("%s;%zu;%zu\n", "IdeVcsFileInfoClass", sizeof(IdeVcsFileInfoClass), alignof(IdeVcsFileInfoClass));
    printf("%s;%zu;%zu\n", "IdeVcsFileStatus", sizeof(IdeVcsFileStatus), alignof(IdeVcsFileStatus));
    printf("%s;%zu;%zu\n", "IdeVcsInitializerInterface", sizeof(IdeVcsInitializerInterface), alignof(IdeVcsInitializerInterface));
    printf("%s;%zu;%zu\n", "IdeVcsInterface", sizeof(IdeVcsInterface), alignof(IdeVcsInterface));
    printf("%s;%zu;%zu\n", "IdeVcsMonitorClass", sizeof(IdeVcsMonitorClass), alignof(IdeVcsMonitorClass));
    printf("%s;%zu;%zu\n", "IdeVcsTagInterface", sizeof(IdeVcsTagInterface), alignof(IdeVcsTagInterface));
    printf("%s;%zu;%zu\n", "IdeWebkitPage", sizeof(IdeWebkitPage), alignof(IdeWebkitPage));
    printf("%s;%zu;%zu\n", "IdeWebkitPageClass", sizeof(IdeWebkitPageClass), alignof(IdeWebkitPageClass));
    printf("%s;%zu;%zu\n", "IdeWorkbenchAddinInterface", sizeof(IdeWorkbenchAddinInterface), alignof(IdeWorkbenchAddinInterface));
    printf("%s;%zu;%zu\n", "IdeWorkbenchClass", sizeof(IdeWorkbenchClass), alignof(IdeWorkbenchClass));
    printf("%s;%zu;%zu\n", "IdeWorkspace", sizeof(IdeWorkspace), alignof(IdeWorkspace));
    printf("%s;%zu;%zu\n", "IdeWorkspaceAddinInterface", sizeof(IdeWorkspaceAddinInterface), alignof(IdeWorkspaceAddinInterface));
    return 0;
}
